/* (C) 2012, 2022 Felix Salfelder
 *
 * This file is part of "Gnucap", the Gnu Circuit Analysis Package.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 */

#include "symbol.h"
#include <globals.h>
/*--------------------------------------------------------------------------*/
GEDA_SYMBOL p1;
DISPATCHER<CARD>::INSTALL d1(&device_dispatcher, "geda_symbol", &p1);
GEDA_PIN p2;
DISPATCHER<CARD>::INSTALL d2(&device_dispatcher, "geda_pin", &p2);
/*--------------------------------------------------------------------------*/
// BUG: lang.
GEDA_PIN::GEDA_PIN( CS& cmd )
{
	COMPONENT* c = this;
	x0() = cmd.ctoi();
	y0() = cmd.ctoi();
	int x = cmd.ctoi();
	int y = cmd.ctoi();
	_bus = cmd.ctob();
	_color = cmd.ctou();
	bool swap = cmd.ctob();
	if (swap){
		x0() = x;
		y0() = y;
	}else{
	}
	std::string    _portvalue="_";
	try{
		cmd.get_line("");
	}catch(Exception_End_Of_Input&){untested();
		return;
	}
	if (cmd >> "{"){
		for(;;){
			try{
				cmd.get_line("");
			}catch(...){ incomplete();
			}
			if(cmd>>"}"){
				try{
					cmd.get_line("");
				}catch(...){ incomplete();
				}
				break;
			}else{
				if (cmd>>"T"){
				} else {
					std::string pname = cmd.ctos("=","","");
					std::string value;
					size_t here = cmd.cursor();
					cmd >> "=";
					if(!cmd.stuck(&here)){
						cmd >> value;
						c->set_param_by_name(pname, value);
					} else {untested();
						untested();
					}
				}
			}
		}
	}
	if(find("pinlabel") == end()){
	}else{
	}
}
/*--------------------------------------------------------------------------*/
COMPONENT* GEDA_SYMBOL::operator>>(COMPONENT* m) const
{
	for(std::set<GEDA_PIN>::const_iterator i=pinbegin(); i!=pinend(); ++i){
		trace0("GEDA_SYMBOL::operator>>");
		std::string l = i->label(); // why std::string&? hmmm
		trace2("GEDA_SYMBOL::operator>>", i->pinseq(), i->label());
		assert(i->pinseq());
		m->set_port_by_index(i->pinseq()-1, l);
	}
	return m;
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
