/* (C) 2012 Felix Salfelder
 *
 * This file is part of "Gnucap", the Gnu Circuit Analysis Package.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 */

#ifndef GEDA_SYMHOL_H
#define GEDA_SYMHOL_H

#include <memory>
#include <gmpxx.h> // to workaround bug in gmp header about __cplusplus

#ifdef HAVE_LIBGEDA_LIBGEDA_H
#define COMPLEX NOCOMPLEX // COMPLEX already came from md.h
extern "C"{ //
# include <libgeda/libgeda.h>
# undef min
# undef max
}
#undef COMPLEX
#endif

#include <io_trace.h>
#include <ap.h>
#include <set>

#include "e_subckt.h"
#include "e_node.h"

enum angle_t {
	a_invalid = -1,
	a_0 = 0,
	a_90 = 90,
	a_180 = 180,
	a_270 = 270
};
/*--------------------------------------------------------------------------*/
enum mirror_t {
	m_invalid = -1,
	m_0 = 0,
	m_r = 1
};
/*--------------------------------------------------------------------------*/
class GEDA_PIN : public COMPONENT, public std::map<std::string, std::string>{
		mutable node_t _pin;
		node_t& n_(int i)const override {assert(!i); return _pin;}
	public:
		GEDA_PIN() : COMPONENT() {
			operator[]("pinlabel") = "unknown";
			operator[]("pinseq") = "0";
		}
		GEDA_PIN(CS& cmd); // BUG
		CARD* clone()const override{return new GEDA_PIN(*this);}
		typedef std::map<std::string, std::string> parent;
		typedef parent::const_iterator const_iterator;
	private: // CARD
		std::string dev_type()const override {
			return "geda_pin";
		}
		std::string value_name()const override {untested();
			return "#";
		}
		std::string port_name(int)const override {untested();
			return "incomplete";
		}
		bool print_type_in_spice()const override {untested();
			return false;
		}
		int set_param_by_name(std::string n, std::string v) override{
			if(n=="pinseq"){
				_pinseq = atoi(v.c_str());
			}else{
			}

			operator[](n) = v; // BUG
			incomplete();
			return 0;
		}
		int param_count() const override{
			return COMPONENT::param_count() + 4;
		}
		std::string param_name(int I) const override{
			switch (I) {
			  case 0:  return "$xposition"; // BUG
			  case 1:  return "$yposition"; // BUG
			  case 2:  return "color";
			  case 3:  return "pinseq";
			  default: return COMPONENT::param_name(I-4);
			}
		}
		std::string param_value(int I) const override{
			switch (I) {
			  case 0:  return std::to_string(_xy.first);
			  case 1:  return std::to_string(_xy.second);
			  case 2:  return std::to_string(_color);
			  case 3:  return std::to_string(_pinseq);
			  default: return COMPONENT::param_value(I-4);
			}
		}
		bool param_is_printable(int I) const override{
			switch (I) {
			  case 0:  return true;
			  case 1:  return true;
			  case 2:  return true;
			  case 3:  return true;
			  default: return COMPONENT::param_is_printable(I-4);
			}
		}
	public:
		int& x0(){return _xy.first;}
		int& y0(){return _xy.second;}
		int x0()const{return _xy.first;}
		int y0()const{return _xy.second;}
		const std::pair<int,int>& X()const { return _xy; }
		//int& x1(){return _xy[2];}
		//int& y1(){return _xy[3];}
		unsigned& color(){return _color;}
		bool& bus(){return _bus;}
	public:
		bool has_key(const std::string key)const{
			const_iterator it = parent::find( key );
			return (it != end());
		}
	public:
		unsigned pinseq()const{
			// the manual says pinseq is mandatory.
			assert(_pinseq!=-1u);
			return _pinseq;
			// assert(find("pinseq") != end()); // for now.
			// return atoi(find("pinseq")->second.c_str());
		}
		std::string label()const{
			if(find("pinlabel") == end()){
				incomplete();
				assert(find("pinseq") != end());
				return( "unknown_pin_" + find("pinseq")->second );
			}
			return find("pinlabel")->second.c_str();
		}
	private:
		std::pair<int,int> _xy;
		unsigned _color;
		bool _bus; //"type of pin"
		unsigned _pinseq{-1u};
		std::string _pinlabel; // label?
};
/*--------------------------------------------------------------------------*/
class GEDA_SYMBOL : public BASE_SUBCKT {
	mutable node_t _nn;
	node_t& n_(int i)const override {assert(0); return _nn;}
	std::string _filename;
public: // BUG
	std::set<GEDA_PIN> _pins;
	std::map<std::string, const GEDA_PIN*> _pins_by_name;
	std::vector<const GEDA_PIN*> _pins_by_seq;
  	std::map<std::string, std::string> _attribs;
private:
	// T and stuff?

	public:
		typedef std::map<std::string, std::string>::iterator iterator;
		typedef std::map<std::string, std::string>::const_iterator const_iterator;
		GEDA_SYMBOL() : BASE_SUBCKT(),
		    _pins(),
		    x(0),
		    y(0),
		    _mirror(0),
		    _angle(a_0) {
			new_subckt();
		}
		~GEDA_SYMBOL(){}

	private: // BASE_SUBCKT
		bool		makes_own_scope()const override {return true;}
	public: // bug?
		CARD_LIST* scope() override {
			return subckt();
		}
	private:
		GEDA_SYMBOL(const GEDA_SYMBOL& p) : BASE_SUBCKT(p),
			_pins(p._pins),
			_pins_by_name(p._pins_by_name), // hmmm.
			_pins_by_seq(p._pins_by_seq), // hmmm.
			_attribs(p._attribs),
			x(p.x),
			y(p.y),
			_mirror(p._mirror),
			_angle(p._angle)
		{
			new_subckt();
		}
//		GEDA_SYMBOL(const GEDA_SYMBOL& x) :
//			std::map<std::string, std::string>(x),
//			_filename(x._filename),
//			_pincount(x._pincount)
//  		{untested();
//			trace1("copying pins", _pins.size());
//			_pins = x._pins;
//		}
	private: // CARD
		bool is_device()const override{
			return false;
		}
		std::string dev_type()const override {
			return "geda_symbol";
		}
		std::string value_name()const override {untested();
			return "#";
		}
		std::string port_name(int)const override {untested();
			return "incomplete";
		}
		bool print_type_in_spice()const override {untested();
			return false;
		}
	public:
		GEDA_SYMBOL* clone()const override {return new GEDA_SYMBOL(*this);}
		bool has_key(const std::string key) const{
			const_iterator it = _attribs.find( key );
			return (it != _attribs.end());
		}
		int ext_nodes()const override  {return _pins.size();}
		const std::string operator[](const std::string& x)const{
			const_iterator it = _attribs.find( x );
			if (it != _attribs.end()) return it->second;
			return "";
		}
		std::string& operator[](const std::string& x){
			return _attribs[x];
		}
		iterator begin(){ return _attribs.begin(); }
		iterator end(){ return _attribs.end(); }
	public: // abuse for symbol instances
		int x;
		int y;
		void push_back(const GEDA_PIN& x) {
			_pins.insert(x);
		}
		std::set<GEDA_PIN>::const_iterator pinbegin()const {
			assert(ext_nodes()==int(_pins.size()));
			return _pins.begin();
		}
		std::set<GEDA_PIN>::const_iterator pinend()const {
			return _pins.end();
		}
		COMPONENT* operator>>(COMPONENT*) const;
		GEDA_PIN const* pin(std::string x){return _pins_by_name[x];}
		GEDA_PIN const* pin(unsigned x){assert(x); return _pins_by_seq[x-1];}
		const angle_t& angle()const {return _angle;}
		bool mirror()const {return _mirror;}
		// BUG?
		void set_angle(angle_t x){_angle=x;}
		void set_mirror(bool x) {_mirror=x;}
	private:
		bool _mirror;
		angle_t _angle;
}; // GEDA_SYMBOL
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
GEDA_SYMBOL* make_GEDA_SYMBOL(std::string); // TODO
/*--------------------------------------------------------------------------*/
// sort of dynamic dispatcher ... (obsolete)
class GEDA_SYMBOL_MAP{ //
	public:
		typedef std::map<std::string, GEDA_SYMBOL*> parent;
		~GEDA_SYMBOL_MAP(){
			for(parent::iterator i = _m.begin(); i!=_m.end(); ++i){
				// incomplete(); // memory leak
				// delete i->second;
			}
		}

	public:
		GEDA_SYMBOL* operator[](const std::string key){
			parent::const_iterator it = _m.find( key );
			GEDA_SYMBOL*& s = _m[key];
			if ( it == _m.end() ) {
				trace1("GEDA_SYMBOL_MAP", key);
				s = make_GEDA_SYMBOL(key); // TODO.
			} else {
			}

			return _m[key];
		}
	private:
	  	std::map<std::string, GEDA_SYMBOL*> _m;
};
/*--------------------------------------------------------------------------*/

#endif // guard
