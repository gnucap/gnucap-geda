/* 
 * Written by Savant Krishna <savant.2020@gmail.com>
 *
 * This file is part of "Gnucap", the Gnu Circuit Analysis Package
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *------------------------------------------------------------------
 * This is the device 'net' : a connection between nodes.
 */
#ifndef D_NET__H
#define D_NET__H
#ifdef COMPLEX
# error COMPLEX mess
#endif
#include <md.h>
#include <e_compon.h>
#include <e_node.h>
#include <u_xprobe.h>
#ifndef HAVE_UINT_T
typedef int uint_t;
#endif

#undef HAVE_COLLAPSE
#define MAX_NET_NODES 99999 // BUG
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
class DEV_NET : public COMPONENT {
  // PARAMETER<std::string> _color; later.
  std::string _color;
public:
  explicit DEV_NET() : COMPONENT() { }
  explicit DEV_NET(const DEV_NET& p);
  ~DEV_NET(){}
private:
  void precalc_first()override;
  void expand()override;
  // void precalc_last();
private:
  void tr_iwant_matrix()override;
// void tr_accept();
  double tr_probe_num(const std::string&)const override;
  XPROBE ac_probe_ext(const std::string& x)const override;
  void ac_iwant_matrix()override;
private:
  bool      param_is_printable(int)const override {return false;}
private:
  char      id_letter()const override {return 'N';}
  std::string value_name()const override {return "";}
  std::string dev_type()const override { return "net";}
  uint_t    max_nodes()const override {return MAX_NET_NODES;}
  uint_t    min_nodes()const override {return 2;}
  uint_t    matrix_nodes()const override {return _net_nodes;}
  uint_t    net_nodes()const override {return _net_nodes;}
  int set_param_by_name(std::string n, std::string v)override {
    if(n=="color"){
      _color = v;
      return 0; //?
    }else{
      return COMPONENT::set_param_by_name(n, v);
    }
  }

  bool      has_iv_probe()const override {return true;}
  CARD*     clone()const override     {return new DEV_NET(*this);}
  bool      print_type_in_spice()const override {return false;}
//		bool do_tr();
  void tr_begin()override;
  void tr_load()override;
  void ac_begin()override;
  void ac_load()override;
protected:
  int param_count()const override  {return COMPONENT::param_count();}
private:
  node_t& n_(int i)const override {
    assert(i<MAX_NET_NODES);
    return _nodes[i];
  }
  std::string port_name(uint_t i)const override {
    assert(i>=0);
    if(i<2){
      static std::string names[]={"p","n"};
      return names[i];
    }else{
      return "m"+to_string(i-1);
    }
  }
  PARAMETER<double> _resistance;
public:
  mutable node_t    _nodes[MAX_NET_NODES];
private:
#ifndef HAVE_COLLAPSE
  double _g0, _g1;
#endif
};
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
#endif
// vim:ts=8:sw=2:et
