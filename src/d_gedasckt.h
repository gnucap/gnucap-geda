/*                     -*- C++ -*-
 * Copyright (C) 2015 Albert Davis (d_subckt.cc)
 *               2015, 2025 Felix Salfelder
 *
 * This file is part of "Gnucap", the Gnu Circuit Analysis Package
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *------------------------------------------------------------------
 * data structures for subcircuits from geda schematics
 */
#include <e_node.h>
#include <e_paramlist.h>
#include <e_subckt.h>
#include <boost/pending/disjoint_sets.hpp>
#include <lang_geda.h>
//#include <m_part.h>

#ifndef HAVE_UINT_T
typedef int uint_t;
#endif

#undef PORTS_PER_SUBCKT
#define PORTS_PER_SUBCKT 100
/*--------------------------------------------------------------------------*/
class MODEL_GEDA_SUBCKT;
/*--------------------------------------------------------------------------*/
class INTERFACE DEV_GEDA_SUBCKT : public BASE_SUBCKT {
  friend class MODEL_GEDA_SUBCKT;
  mutable node_t _nodes[PORTS_PER_SUBCKT];
private:
  explicit	DEV_GEDA_SUBCKT(const DEV_GEDA_SUBCKT&);
public:
  typedef boost::disjoint_sets_with_storage<> PARTITION;
public:
  explicit	DEV_GEDA_SUBCKT();
		~DEV_GEDA_SUBCKT();
private: // from DEV_SUBCKT
  char		id_letter()const override	{return 'X';}
  bool		print_type_in_spice()const override {return true;}
  std::string   value_name()const override	{return "#";}
  // std::string   dev_type()const
  uint_t	max_nodes()const override	{return PORTS_PER_SUBCKT;}
  node_t& n_(int i)const override {
    assert(i<PORTS_PER_SUBCKT);
    return _nodes[i];
  }
#ifdef NDEBUG
  void map_nodes(){ untested();
    untested();
    trace3("map_nodes", long_label(), net_nodes(), matrix_nodes());
    BASE_SUBCKT::map_nodes();
  }
#endif
  uint_t	min_nodes()const override	{return 0;}
  uint_t	matrix_nodes()const override	{return 0;}
public: //HACK
  uint_t	net_nodes()const override	{return _net_nodes;}
private:
//  CARD*	clone_instance()const;
  void		precalc_first()override;
  bool		makes_own_scope()const override  {itested(); return false;}

  void		expand()override;
private:
  void		precalc_last()override;
  double	tr_probe_num(const std::string&)const override;
  int param_count_dont_print()const override {return common()->COMMON_COMPONENT::param_count();}

  std::string port_name(uint_t i)const override;
  std::string port_default(uint_t i)const;
  CARD*	clone()const override		{return new DEV_GEDA_SUBCKT(*this);}
public:
  void set_port_by_index(uint_t num, std::string& ext_name)override;
private: // node stuff
  void map_subckt_nodes(const CARD* model);
public:
  void collapse_nodes(const NODE* a, const NODE* b);
  void orbit_number(unsigned* map, unsigned len, unsigned* port);
//  void map_net(unsigned rep, unsigned to, unsigned* map);
  void apply_map(unsigned* map);

#if 0
  std::string port_name(int i)const {itested();
    if (_parent) {itested();
      return _parent->port_value(i);
    }else{itested();
      return "";
    }
  }
#endif
private:
  void default_connect(const CARD* model);
  void set_parent(const MODEL_GEDA_SUBCKT* p);
  PARTITION *_part;
  unsigned *_map;
  const MODEL_GEDA_SUBCKT* _parent;
  unsigned _num_cc; // number connected components
private: // cleanup later
  void map_a_node(std::string x);
  unsigned _num_auto_nodes; // number of nodes added during precalc_first.
};
/*--------------------------------------------------------------------------*/
// proto? move to .cc
class INTERFACE MODEL_GEDA_SUBCKT : public DEV_GEDA_SUBCKT {
private:
  explicit	MODEL_GEDA_SUBCKT(const MODEL_GEDA_SUBCKT&p);
public:
  explicit	MODEL_GEDA_SUBCKT();
		~MODEL_GEDA_SUBCKT();
public: // override virtual
  char id_letter()const	override {return '\0';}
  CARD* clone_instance()const override;
  CARD* clone()const override		{return new MODEL_GEDA_SUBCKT(*this);}
  void set_port_by_index(uint_t num, std::string& ext_name, std::string default_net="");
  void set_port_by_index(uint_t num, std::string& ext_name)override {
    set_port_by_index(num, ext_name, "");
  }
  bool		makes_own_scope()const override {return true;}
  CARD_LIST*	   scope()override		{assert(subckt()); return subckt();}
  const CARD_LIST* scope()const override	{assert(subckt()); return subckt();}
private: // no-ops for prototype
  bool is_device()const	override {return false;}
  void precalc_first()override {}
  void expand()override {}
  void precalc_last()override {}
  void map_nodes()override {
    trace1("map_nodes base", long_label());
  }
  void tr_begin()override {}
  void tr_load()override {}
  TIME_PAIR tr_review()override {return TIME_PAIR(NEVER, NEVER);}
  void tr_accept()override {}
  void tr_advance()override {}
  void tr_restore()override {untested();}
  void tr_regress()override {}
  void dc_advance()override {}
  void ac_begin()override {}
  void do_ac()override {}
  void ac_load()override {}
  bool do_tr()override {return true;}
  bool tr_needs_eval()const override{ return false;}
  void tr_queue_eval()override {}
  void tr_final()override {}
  void dc_final()override {}
  void ac_final()override {}
  int set_port_by_name(std::string& name, std::string& value) override;
  std::string port_name(int i)const override
	{return (_port_name[i]!="") ? _port_name[i] : port_value(i);}
  std::string const port_value(int i)const override;
private:
  std::vector<std::string> _port_name; // [PORTS_PER_SUBCKT];
  std::vector<std::string> _defaults;
  std::string _defconn; // PARAMETER<string>...?
public: // hmm, friends?
  std::string defconn() const{return _defconn;}
  void set_defconn(std::string x){_defconn=x;}
  std::string port_default(uint_t i)const;
}; // MODEL_GEDA_SUBCKT
/*--------------------------------------------------------------------------*/
inline int MODEL_GEDA_SUBCKT::set_port_by_name(std::string& name, std::string& value)
{
  int index = net_nodes();
  if(index<int(_port_name.size())) { untested();
  }else{
    _port_name.resize(index+1);
  }
  _port_name[index] = name;
  trace3("M::spbn", index, name, value);
  set_port_by_index(index, value, ""); // bumps _net_nodes
  return index;
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
// vim:ts=8:sw=2:noet:
