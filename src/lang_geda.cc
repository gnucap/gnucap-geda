/* This file is the plugin for the gEDA-gschem plugin
 * The documentation of how this should work is at bit.ly(slash)gnucapwiki
 *
 * This file is part of "Gnucap", the Gnu Circuit Analysis Package.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 */

#define ADD_VERSION
#include <l_lib.h>
#include <l_dispatcher.h>
#include <globals.h>
#include <c_comand.h>
#include <d_dot.h>
#include <d_coment.h>
#include <e_paramlist.h>
#include <e_model.h>
#include <u_lang.h>
#include <u_nodemap.h>

#include <fts.h>
#include <gmpxx.h> // to workaround bug in gmp header about __cplusplus

#ifdef HAVE_LIBGEDA_LIBGEDA_H
#define COMPLEX NOCOMPLEX // COMPLEX already came from md.h
extern "C"{ //
# include <libgeda/libgeda.h>
}
#undef COMPLEX
#endif

#include "symbol.h"
#include "d_net.h"
#include "d_place.h"
#include "d_gedasckt.h"
#include "io_trace.h"

/*--------------------------------------------------------------------------*/
#ifndef USE
#define USE(a) (void) a;
#endif
/*--------------------------------------------------------------------------*/
#ifndef MODEL_SUBCKT
#define MODEL_SUBCKT BASE_SUBCKT
#endif
/*--------------------------------------------------------------------------*/
#define DUMMY_PREFIX std::string("!_")
/*--------------------------------------------------------------------------*/
namespace geda{ //
/*--------------------------------------------------------------------------*/
std::string int_prefix="x_";
/*--------------------------------------------------------------------------*/
class LANG_GEDA : public LANGUAGE {
	friend class CMD_GEDA;
	friend class CMD_C;
#ifdef HAVE_LIBGEDA_LIBGEDA_H
	TOPLEVEL* pr_current;
#endif

	struct portinfo { //
		portinfo(std::string n, int a, int b): name(n), x(a), y(b) {}
		std::string name;
		int x,y;
	};
	struct netinfo { //
		netinfo(int _x0, int _y0, int _x1, int _y1, unsigned c):
			x0(_x0), y0(_y0), x1(_x1), y1(_y1), color(c) {}
		int x0, y0, x1, y1;
		unsigned color;
	};
	mutable std::queue<netinfo> _netq;
	mutable GEDA_SYMBOL* _C; //stashes C command and body (HACK/workaround)
public:
	LANG_GEDA() : LANGUAGE(), _C(NULL){
		trace0("gedainit");
#ifdef HAVE_LIBGEDA_LIBGEDA_H
		scm_init_guile(); // urghs why?
		libgeda_init();
		pr_current = s_toplevel_new ();
		g_rc_parse(pr_current, "gschemrc", NULL, NULL);
		// i_vars_set (pr_current); // why?
#endif
	}

private:
	mutable bool _gotline_sym;
	public:
	enum MODE {mATTRIBUTE, mCOMMENT} _mode;
	mutable int _no_of_lines;
	mutable bool _gotline;
	mutable std::string _componentname;
	std::string name()const override {return "gschem";}
	bool case_insensitive()const override {return false;}
	UNITS units()const override {return uSI;}

public: // obsolete functions to be declared
	std::string arg_front()const override {untested();
		return " "; //arbitrary
	}
	std::string arg_mid()const override {untested();
		return "="; //arbitrary
	}
	std::string arg_back()const override {untested();
		return "";  //arbitrary
	}

public:
	void		  parse_top_item(CS&, CARD_LIST*)override;
	void parse_item_(CS& cmd, CARD* owner, CARD_LIST*) const;
	DEV_COMMENT*  parse_comment(CS&, DEV_COMMENT*)override;
	DEV_DOT*	  parse_command(CS&, DEV_DOT*)override;
	MODEL_CARD*	  parse_paramset(CS&, MODEL_CARD*)override;
	MODEL_SUBCKT* parse_module(CS&, MODEL_SUBCKT*)override;
	COMPONENT*    parse_componmod(CS&, COMPONENT*);
	COMPONENT*	  parse_instance(CS&, COMPONENT*)override;
	std::string*  parse_pin(CS& cmd, COMPONENT* x, int index, bool ismodel)const;
	std::string	  find_type_in_string(CS&)const;
	// gnucap backwards compatibility
	std::string	  find_type_in_string(CS&x)override {return const_cast<const LANG_GEDA*>(this)->find_type_in_string(x);}
	void parse_net(CS& cmd, COMPONENT* x);
	void parse_place(CS& cmd, COMPONENT* x);

private:
	void print_paramset(OMSTREAM&, const MODEL_CARD*)override;
	void print_module(OMSTREAM&, const MODEL_SUBCKT*)override;
	void print_instance(OMSTREAM&, const COMPONENT*)override;
	void print_comment(OMSTREAM&, const DEV_COMMENT*)override;
	void print_command(OMSTREAM& o, const DEV_DOT* c)override;
	void print_component(OMSTREAM& o, const COMPONENT* x);
	void print_net(OMSTREAM& o, const COMPONENT* x);
	void print_node_xy(OMSTREAM& o, const COMPONENT* c, int portindex);

	GEDA_SYMBOL* parse_C(CS& cmd)const;
	void parse_component(CS& cmd,COMPONENT* x);
public: // TODO
	void parse_symbol_file(CARD* x, CS&) const;
private:
	void make_node_at(portinfo const& p, CARD_LIST* scope);
	std::string find_node_name(int x, int y, CARD_LIST const* scope) const;
	void store_node_position(NODE const*, CARD_LIST const* scope);
	bool store_node_position(int i, COMPONENT const* c, NODE const*);
	bool node_is_at(NODE* n, int x, int y, CARD_LIST const* scope);
	void store_attributes(std::string attrib_string, tag_t x);
	DEV_DOT* parse_symbol_file(DEV_DOT*, const GEDA_SYMBOL&)const;
	const std::string connect_place(CARD* x, int cx, int cy);
	const NODE* find_place(const CARD* x, int xco, int yco);
	const NODE* find_place(const CARD* x, std::string xco, std::string yco);
	const NODE* find_place(const CARD* x, std::string name);
	std::pair<int, int> find_place_(const CARD* x, std::string name);
	std::string* find_place_string(const CARD* x, std::string name);
	void connect_net(COMPONENT *net, int x0, int y0, int x1, int y1);
	void connect_to_net(const CARD *place, std::string name, int x0, int y0);
	void connect_if_needed(int x0, int y0, int x1, int y1,
	                       int n1x, int n1y, int n2x, int n2y) const;
	void connect_if_needed(int x0, int y0, COMPONENT* net, std::string wirename);
	static void read_file(std::string, CARD_LIST* Scope, BASE_SUBCKT* owner=NULL);
	static void read_spice(std::string, CARD_LIST* Scope, BASE_SUBCKT* owner=NULL);
	static CARD_LIST::const_iterator find_nondevice(std::string name, CARD_LIST* Scope=NULL);
	static CARD_LIST::const_iterator find_card(std::string name, CARD_LIST* Scope=NULL, bool model=0);

	void set_port_by_index(COMPONENT* c, int idx, std::string const& value, int x, int y);

private: // node suff
	int get_(NODE const* n, std::string x) const;
	int get_x(NODE const* n) const;
	int get_y(NODE const* n) const;
public:
	GEDA_SYMBOL const* find_symbol(std::string const& basename) const;

private:
	static GEDA_SYMBOL_MAP _symbol;
	static unsigned _nodenumber;
	static unsigned _netnumber;
	// put fake models here?
	// static CARD_LIST _symbols;
	std::string _defconn;
}lang_geda;
DISPATCHER<LANGUAGE>::INSTALL
d(&language_dispatcher, lang_geda.name(), &lang_geda);
/*----------------------------------------------------------------------*/
GEDA_SYMBOL const* LANG_GEDA::find_symbol(std::string const& name) const
{
	trace1("find_symbol", name);
	CARD* c = device_dispatcher[name];
	if(auto s = dynamic_cast<GEDA_SYMBOL const*>(c)){ untested();
		return s;
	}else{
	}
	// CARD* c = symbol_dispatcher[name]; //?
	CARD_LIST::const_iterator i = CARD_LIST::card_list.find_(name);
	if(i == CARD_LIST::card_list.end()){
	}else if(auto s = dynamic_cast<GEDA_SYMBOL const*>(*i)){ untested();
		return s;
	}else{ untested();
	}

#if 1
	// obsolete.
	return _symbol[name];
#else
	GEDA_SYMBOL* s = make_GEDA_SYMBOL(name);
	CARD_LIST::card_list.push_back(s);
	return s;
#endif
}
/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
class CMD_GSCHEM : public CMD { //
public:
  void do_it(CS&, CARD_LIST* Scope)override {
    command("options lang=gschem", Scope);
  }
} p9;
DISPATCHER<CMD>::INSTALL d9(&command_dispatcher, "gschem", &p9);
/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
GEDA_SYMBOL_MAP LANG_GEDA::_symbol;
/*----------------------------------------------------------------------*/
unsigned LANG_GEDA::_netnumber, LANG_GEDA::_nodenumber;
/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
// FIXME: need something like
//std::string LANGUAGE::getlines(FILE *fileptr) const
//to detect { } bodies correctly
// current workaround: read into _C and _gotline hack.
/*----------------------------------------------------------------------*/
//Finds type from find_type_in_string
static void parse_type(CS& cmd, CARD* x)
{
	assert(x);
	std::string new_type;
	new_type=lang_geda.find_type_in_string(cmd);
	x->set_dev_type(new_type);
}
/*--------------------------------------------------------------------------*/
std::string* LANG_GEDA::parse_pin(CS& cmd, COMPONENT* x, int index, bool ismodel)const
{ untested();
	//assert(x); can parse NULL also
	trace0("Got into parse_pin");
	assert( find_type_in_string(cmd) =="pin");
	std::string dump;
	cmd>>"P";
	std::string* coord = new std::string[3];
	if (!ismodel){ untested();
		std::string pinattributes[7];
		for(int i=0;i<7;i++){ untested();
			cmd>>" ">>pinattributes[i];
		}
		if (pinattributes[6]=="1"){ untested();
			coord[0]=pinattributes[2];
			coord[1]=pinattributes[3];
		}else if (pinattributes[6]=="0"){ untested();
			coord[0]=pinattributes[0];
			coord[1]=pinattributes[1];
		}
	}
	else{untested();
		cmd>>dump;
	}
	std::string    _portvalue="_";
	static unsigned number;
	try{ untested();
		cmd.get_line("");
	}catch(Exception_End_Of_Input&){untested();
		return NULL;
	}
	std::string temp=(cmd.fullstring()).substr(0,1);
	if(cmd.match1('{')){ untested();
		cmd>>"{";
		for(;;){ untested();
			cmd.get_line("");
			if(cmd>>"}"){ untested();
				break;
			}else if (cmd>>"T"){ untested();
				cmd>>dump;
			}else{ untested();
				std::string _pname=cmd.ctos("=","",""),_pvalue;
				cmd>>"=">>_pvalue;
				if(_pname=="pinlabel"){ untested();
					_portvalue=_pvalue+_portvalue;
				}else if (_pname=="pintype"){ untested();
					_portvalue=_portvalue+_pvalue;
				}
			}
		}
	}
	if(ismodel and x){ untested();
		std::string portvalue = "np_" + _portvalue+::to_string(number++);

		incomplete(); // position?
		x->set_port_by_index(index, portvalue);

		return NULL;
	}else{ untested();
		return coord;
	}
}
/*--------------------------------------------------------------------------*/
// FIXME: do symbol_type?
// BUG: returns coords...
void LANG_GEDA::parse_symbol_file(CARD* x, CS& cmd) const
{
	assert(!_C);
	assert(!_netq.size());
	_gotline_sym = 0;
//	COMPONENT* c = dynamic_cast<COMPONENT*>(x);
	GEDA_SYMBOL* s = dynamic_cast<GEDA_SYMBOL*>(x);
	assert(s);
	// MODEL_SUBCKT* m = dynamic_cast<MODEL_SUBCKT*>(x);
//	std::string dump;

	// name of the symbol category (collection name)
	// const char* sourcename = s_clib_source_get_name (s_clib_symbol_get_source(symbol));
	//
	// the file contents as string (wtf?!)
	// char* data = s_clib_symbol_get_data(symbol);

	//trace2("LANG_GEDA::parse_symbol_file", basename, filename);

#if 1
	CARD_LIST* cl = s->subckt();
	assert(cl);
	unsigned scope = 0;
	while(true){
		if (cmd.match1('{')) {untested();
			scope++;
		} else if(cmd.match1('}')) {untested();
			assert(scope);
			scope--;
			continue;
		}

		if (!scope){
			if (cmd.umatch("P ")){
				// waah unclever...
				GEDA_PIN p(cmd);

				p.set_label(p.label());

				if(!p.has_key("pinseq")) { untested();
					std::string basename = "TODO_basename"; // ask sym_cmd?
					error(bWARNING, "pin without pinseq in %s\n", basename.c_str());
					p["pinseq"] = to_string(unsigned(1+s->_pins.size()));
				} else {
				}
				// _pins.resize(max(p.pinseq(), _pins.size()+1));
				assert(p.pinseq()); // starts at 1?
				std::pair<std::set<GEDA_PIN>::const_iterator,bool> f = s->_pins.insert(p);
				if(f.second){
				}else{incomplete();
					error(bDANGER,"pin collision %d %s\n", p.pinseq(), p.label().c_str());
					// collision
				}
				const GEDA_PIN* P = &(*(f.first));
				s->_pins_by_name[p.label()] = P;

				if(p.pinseq()<=s->_pins_by_seq.size()){
					// ok
				}else{
					s->_pins_by_seq.resize(p.pinseq());
				}
				s->_pins_by_seq[p.pinseq()-1] = P;
				CARD* pcl = p.clone();
				pcl->set_owner(nullptr);
				cl->push_back(pcl);
				continue; // line has been read
			}else{
				incomplete();
			}
			{
				std::string pname = cmd.ctos("=","","");
				size_t here = cmd.cursor();
				std::string pvalue;
				cmd >> "=";
				if(!cmd.stuck(&here)){
					cmd >> pvalue;
					if(pvalue!=""){
						s->_attribs[pname] = pvalue;
					}else{ untested();
					}
				}else{
				}
			}
		}
		try{
			cmd.get_line("");
		}catch (Exception_End_Of_Input&){
			break;
		}
	}
#else
	//Now parse the sym_cmd which will get lines
	int index=0;
	while(true){ untested();
		try{ untested();
			if (!_gotline_sym) sym_cmd.get_line("");
		}catch (Exception_End_Of_Input&){ untested();
			break;
		}
		std::string linetype = find_type_in_string(sym_cmd);
		// trace2("LANG_GEDA::parse_symbol_file", linetype, sym_cmd.fullstring());
		bool ismodel=false;
#if 0
		if (x && x->short_label()==DUMMY_PREFIX+basename){untested();
			ismodel=true;
		}else{ untested();
		}
#endif
		if (linetype=="dev_comment"){ untested();
			// nop
		}else if (linetype=="pin" && (c || !x)){ untested();
			// trace2("parse_symbol_file parsing pin", basename, sym_cmd.fullstring());
			parse_pin(sym_cmd,c,index++,ismodel);
			// coord.push_back();
			// trace2("parse_symbol_file pin done", basename, sym_cmd.fullstring());
		}else if(linetype=="pin"){untested();
			// pin. this is a device, but we are in command mode
			coord.push_back(new std::string("foo"));
			return;
		}else if(linetype=="graphical"){untested();
			sym_cmd>>"graphical=";
			std::string value;
			sym_cmd>>value;
			if(value=="1"){untested();
				trace0("graphical");
				return;
			}else{ untested();
			}
		}else if(linetype=="file"){untested();
			trace2("parse_symbol_file", sym_cmd.fullstring(), linetype);
			sym_cmd>>"file=";
			sym_cmd>>dump;
			DEV_DOT* d = dynamic_cast<DEV_DOT*>(x);
			if(d && dump != "?"){untested();
				d->set(d->s() + " " + dump);
			}else{ untested();
			}
		}else if(m && linetype=="refdes"){ untested();
			// skip
		}else if(c && linetype=="refdes"){ untested();
			sym_cmd >> "refdes=";
			sym_cmd >> dump;
			x->set_label(dump);
		}else if(linetype=="device"){ untested();
			sym_cmd>>"device=";
			sym_cmd>>dump;
			DEV_DOT* d = dynamic_cast<DEV_DOT*>(x);
			if(d){untested();
				d->set(dump);
			}else if( (c = dynamic_cast<COMPONENT*>(x) )){ untested();
				// c->set_label(dump);
			}else{untested();
				incomplete();
			}
		}else if(c && linetype != "" ){ untested();
			sym_cmd >> linetype;
			size_t here = sym_cmd.cursor();
			sym_cmd >> "=";
			if(!sym_cmd.stuck(&here)) { untested();
				try { untested();
					sym_cmd >> dump;
					x->set_param_by_name(linetype, dump);
				} catch (Exception_No_Match&) { untested();
				} catch (Exception_Too_Many&) { untested();
				}
			}else{ untested();
			}
		} else {untested();
			sym_cmd>>dump;
			trace3("pa dump", sym_cmd.fullstring(), linetype, dump);
		}
	}
	trace0("done symbol");
#endif
}
/*--------------------------------------------------------------------------*/
void LANG_GEDA::make_node_at(portinfo const& p, CARD_LIST* scope)
{
	// incomplete. detect collisions.
	std::string nodename = std::string(INT_PREFIX) + p.name;
	assert(scope);
	NODE* n = scope->nodes()->new_node(nodename);
	assert(n);
	std::string attr = "geda_x="+to_string(p.x) +
	                 ", geda_y="+to_string(p.y);
	trace2("make_node_at", nodename, attr);
	store_attributes(attr, n->id_tag());
	trace2("made_node_at", p.name, attributes(n->id_tag())->string(id_tag()));
}
/*--------------------------------------------------------------------------*/
std::string LANG_GEDA::find_node_name(int x, int y, CARD_LIST const* scope) const
{
	assert(scope->nodes());
	for(auto& p: *scope->nodes()){
		NODE const* n = p.second;
		assert(n);
		if(!has_attributes(n->id_tag())){
		}else if(attributes(n->id_tag())->operator[]("geda_x") != to_string(x)){
			trace2("find_node_miss", attributes(n->id_tag())->operator[]("geda_x"), to_string(x));
		}else if(attributes(n->id_tag())->operator[]("geda_y") != to_string(y)){
			trace2("find_node_miss", attributes(n->id_tag())->operator[]("geda_y"), to_string(y));
		}else{
			assert(p.first == n->short_label());
			trace3("find_node_hit", x, y, p.first);
			return p.first;
		}
	}

	return "";
}
/*--------------------------------------------------------------------------*/
//place <nodename> x y
void LANG_GEDA::parse_place(CS& cmd, COMPONENT* place)
{
	trace2("parse_place", place->long_label(), cmd.fullstring());
	assert(place);
	assert(find_type_in_string(cmd)=="place");
	if ( cmd.umatch("place") ) {untested();
		incomplete();
		cmd >> "place";
		std::string _portname;
		int _x, _y;
		cmd >> " " >> _portname >> " " >> _x >> " " >> _y;
		place->set_param_by_name("x",to_string(_x));
		place->set_param_by_name("y",to_string(_y));
		std::string portname = std::string(INT_PREFIX) + "np_" + _portname;
		set_port_by_index(place, 0, portname, _x, _y);
		place->set_label( to_string(_x) + ":" + to_string(_y));
		make_node_at(portinfo("np_" + _portname, _x, _y), place->scope());
	} else {untested();
		trace1("parse_place, huh?", cmd.fullstring());
		unreachable();
	}
}
/*--------------------------------------------------------------------------*/
const NODE* LANG_GEDA::find_place(
		const CARD* x, std::string xco, std::string yco)
{untested();
	return find_place(x, atoi(xco.c_str()), atoi(yco.c_str()));
}
/*--------------------------------------------------------------------------*/
// node is is at c port i. copy over position info, if available.
// return: success.
bool LANG_GEDA::store_node_position(int i, COMPONENT const* c, NODE const* n)
{
	// TODO: flag conflicts.
	if(!has_attributes(c->id_tag())){
		return false;
	}else{
		std::string x;
		try{
			x = attributes(c->id_tag())->at("S0_x"+to_string(i+1));
		}catch(std::out_of_range const&){
			try{
				x = attributes(c->id_tag())->at("S0_x_"+c->port_name(i));
			}catch(std::out_of_range const&){
			}
		}
		std::string y;
		try{
			y = attributes(c->id_tag())->at("S0_y"+to_string(i+1));
		}catch(std::out_of_range const&){
			try{
				y = attributes(c->id_tag())->at("S0_y_"+c->port_name(i));
			}catch(std::out_of_range const&){
			}
		}


		bool success = y.size() && x.size();
		std::string attribs;
		if(y.size() && x.size()){
			attribs = "geda_x=" + x + ", geda_y=" + y;
		}else{
			attribs = "geda_noplace=1";
		}
		trace4("snp", c->long_label(), i, n->short_label(), attribs);
		store_attributes(attribs, n->id_tag());
		trace2("found_node_at", n->short_label(), attributes(n->id_tag())->string(n->id_tag()));
		return success;
	}
}
/*--------------------------------------------------------------------------*/
void LANG_GEDA::store_node_position(NODE const* n, CARD_LIST const* scope)
{
	bool done=false;
	if(!has_attributes(n->id_tag())){
	}else if( attributes(n->id_tag())->operator[]("geda_xyfail") == "1") { untested();
		// failed previuously.
		return;
	}else{
		try{
			attributes(n->id_tag())->at("geda_x");
			attributes(n->id_tag())->at("geda_y");
			done = true;
		}catch(std::out_of_range const&){ untested();
		}
	}

	if(!done) {
		for(CARD_LIST::const_iterator ci=scope->begin(); ci!=scope->end(); ++ci) {
			for(int i=0; i<(*ci)->net_nodes(); ++i){
				auto p = dynamic_cast<COMPONENT const*>(*ci);
				if(!p){ untested();
				}else if(p->port_value(i) != n->short_label()) {
				}else if(store_node_position(i, p, n)) {
					return;
				}else{
				}
			}
		}
		// could not find place anywhere.
		store_attributes("geda_xyfail=1", n->id_tag());
	}else{
	}

}
/*--------------------------------------------------------------------------*/
bool LANG_GEDA::node_is_at(NODE* n, int x, int y, CARD_LIST const* scope)
{ untested();
	if(!has_attributes(n->id_tag())) { untested();
		store_node_position(n, scope);
		return node_is_at(n, x, y, scope);
	}else if(attributes(n->id_tag())->operator[]("geda_place") == "1"){ untested();
		std::string xs = attributes(n->id_tag())->operator[]("geda_x");
		std::string ys = attributes(n->id_tag())->operator[]("geda_y");
		if(xs == to_string(x) && ys == to_string(y)){ untested();
			return true;
		}else{ untested();
			return false;
		}
	}else if(attributes(n->id_tag())->operator[]("geda_xyfail") == "1"){ untested();
		return false;
	}else{ untested();
		unreachable();
		store_node_position(n, scope);
		return node_is_at(n, x, y, scope);
	}
}
/*--------------------------------------------------------------------------*/
const NODE* LANG_GEDA::find_place(const CARD* x, int xco, int yco)
{ untested();
	const CARD_LIST* scope;
	if(x->owner()){ untested();
		scope = x->owner()->scope();
	}else{ untested();
		scope = x->scope();
	}
	for(auto n: *scope->nodes()){ untested();
		if(node_is_at(n.second, xco, yco, scope)) { untested();
			return n.second;
		}else{ untested();
		}
	}

	incomplete();
	return NULL;
}
/*--------------------------------------------------------------------------*/
static bool in_order(int a, int b, int c)
{
	if (a<b){
		return b<c;
	}else if (b<a){
		return c<b;
	}
	return false;
}
/*--------------------------------------------------------------------------*/
static bool on_line(int x1, int y1,
		int n1x, int n1y, int n2x, int n2y)
{
	return ((x1-n1x)*(n2y-n1y)) == ((y1-n1y)*(n2x-n1x));
}
/*--------------------------------------------------------------------------*/
void LANG_GEDA::connect_if_needed(int x0, int y0, COMPONENT* net, std::string wirename)
{
	std::string pv0 = net->port_value(0);
	std::string pv1 = net->port_value(1);
	NODE const* n1 = find_place(net, pv0);
	assert(n1);
	NODE const* n2 = find_place(net, pv1);
	assert(n2);
	int n1x = get_x(n1);
	int n1y = get_y(n1);
	int n2x = get_x(n2);
	int n2y = get_y(n2);
	if(n1x > n2x){
		std::swap(n1x, n2x);
	}else{
	}
	if(n1y > n2y){
		std::swap(n1y, n2y);
	}else{
	}
	if (n1x==n2x && n1y==n2y) { untested();
		unreachable();
		// is this allowed in .sch?!
		error(bDANGER,"singular net in %s, %s-%s\n", net->long_label().c_str(),
				pv0.c_str(), pv1.c_str());
	}else if ( y0 == n2y && y0 == n2y && in_order(n1x, x0, n2x) ) {
		int idx = net->net_nodes();
		net->set_port_by_index(idx, wirename);
		_netnumber++; // retain old numbers.
		// _netq.push( netinfo( x0, y0, n1x, n1y, 4 ));
	}else if ( x0 == n1x && x0 == n2x && in_order(n1y, y0, n2y) ) {
		int idx = net->net_nodes();
		net->set_port_by_index(idx, wirename);
		_netnumber++; // retain old numbers.
		// _netq.push( netinfo( x0, y0, n1x, n1y, 4 ));
	}else{
	}
}
/*--------------------------------------------------------------------------*/
#if 0
//queue an extranet if x0 y0 or x1 y1 is between n1--n2
// only works if there is a net n1--n2
// TODO: avoid recursion more efficiently
void LANG_GEDA::connect_if_needed(int x0, int y0, int x1, int y1,
		int n1x, int n1y, int n2x, int n2y) const
{
	trace8("connect", x0, y0, x1, y1, n1x, n1y, n2x, n2y);
	assert(x0<=x1);
	trace4("",x0-n1x,n2y-n1y, y0-n1y,n2x-n1x);

	int Yl = std::min(y0, y1);
	int Yh = std::max(y0, y1);

	if ( x0 == n1x && y0 == n1y) {
		// stupid. should not be here
	}else if ( x1 == n1x && y1 == n1y) {
		// stupid. should not be here
	}else if ( x0 == n2x && y0 == n2y) {
		// stupid. should not be here
	}else if ( x1 == n2x && y1 == n2y) {
		// stupid. should not be here
	}else if ( x1 < n1x && x1 < n2x){
		// net is too far right
	}else if ( x0 > n1x && x0 > n2x){
		// net is too far left
	}else if ( Yh < n1y && Yh < n2y){
		// net is too far up
	}else if ( Yl > n1y && Yl > n2y){
		// net is too far down
	}else if (y0 == y1 && x0 == x1){
		// connect a pin to the interior of a net
		if( on_line(x0, y0, n1x, n1y, n2x, n2y)){
		//	_netq.push( netinfo( x0, y0, n1x, n1y, 4 ));
					_netnumber++;
		}else{
		}
	}else if( (n2y-n1y)*(x1-x0) == (y1-y0)*(n2x-n1x) ) {
		// same angle, don't do anything
	}else if( on_line(x0, y0, n1x, n1y, n2x, n2y)){
		// x0,y0 is on net
		assert(x0!=n1x || y0!=n1y);
	//	_netq.push( netinfo( x0, y0, n1x, n1y, 4 ));
					_netnumber++;
	}else if( on_line(x1, y1, n1x, n1y, n2x, n2y)){
		// x1,y1 is on net
		assert(x1!=n1x || y1!=n1y);
	//	_netq.push( netinfo( x1, y1, n1x, n1y, 4 ));
					_netnumber++;
	}else if (n1x == n2x && (y0 == y1)){
		// new net is horizontal, found a vertical net
		if (in_order( n2y, y1, n1y)){
			if (n1x == x0){ untested();
				assert( y0 !=  n1y);
			//	_netq.push( netinfo( x0, y0, n1x, n1y, 4 ));
					_netnumber++;
			} else if (n2x == x1){ untested();
				assert( y1 !=  n1y);
					_netnumber++;
			//	_netq.push( netinfo( x1, y1, n1x, n1y, 4 ));
			}
		}
	}else if (n1y == n2y && (x0 == x1)){
		// new net is vertical. found a horizontal net.
		if (in_order(n1x, x1, n2x)){
			if (n1y == y0){ untested();
				assert(x0 !=  n1x);
			//	_netq.push( netinfo( x0, y0, n1x, n1y, 4 ));
					_netnumber++;
			} else if (n2y == y1){ untested();
				assert( x1 !=  n1x);
				//_netq.push( netinfo( x1, y1, n1x, n1y, 4 ));
					_netnumber++;
			}
		}
	}else{
	}
}
#endif
/*--------------------------------------------------------------------------*/
int LANG_GEDA::get_(NODE const* n, std::string x) const
{
	std::string a = attributes(n->id_tag())->operator[]("geda_" + x);
	return atoi(a.c_str());
}
int LANG_GEDA::get_x(NODE const* n) const
{
	return get_(n, "x");
}
int LANG_GEDA::get_y(NODE const* n) const
{
	return get_(n, "y");
}
/*--------------------------------------------------------------------------*/
// connect a newly created place to possibly incident nets
void LANG_GEDA::connect_to_net(const CARD *place, std::string netname, int x0, int y0)
{
	assert(place);
	trace3("LANG_GEDA::connect", place->long_label(), x0, y0);
	CARD_LIST const* scope = place->owner()?place->owner()->scope():place->scope();
	for(CARD_LIST::const_iterator ci = scope->begin(); ci != scope->end(); ++ci) {
		if(DEV_NET* net=dynamic_cast<DEV_NET*>(*ci)){
			// connect end points of place hitting other nets
			if((*ci)->net_nodes()<2){
				// not necessary, as there is a place adjacent to the port.
				continue;
			}else if((net->port_value(0)+"AA").substr(0, INT_PREFIX.length()) != INT_PREFIX) { untested();
				// rail...?
				continue;
			}else if((net->port_value(1)+"AA").substr(0, INT_PREFIX.length()) != INT_PREFIX) {
				continue;
			}else{
			}

#if 1
			connect_if_needed(x0, y0, net, netname);
#else
			std::string pv0 = net->port_value(0);
			std::string pv1 = net->port_value(1);
			assert(pv0 != pv1);
			// FIXME: faster...
			incomplete();
			NODE const* n1 = find_place(net, pv0);
			assert(n1);
			NODE const* n2 = find_place(net, pv1);
			assert(n2);
			if (get_x(n1)==get_x(n2) && get_y(n1)==get_y(n2)) { unreachable();
				// is this allowed in .sch?!
				error(bDANGER,"singular net in %s, %s-%s\n", place->long_label().c_str(),
						pv0.c_str(), pv1.c_str());
			}else{
				int n1x = get_x(n1);
				int n2x = get_x(n2);
				int n1y = get_y(n1);
				int n2y = get_y(n2);
				connect_if_needed(x0, y0, x0, y0, n1x, n1y, n2x, n2y);
			}
#endif
		}
	}
}
/*--------------------------------------------------------------------------*/
// connect a newly created net to possibly incident items
void LANG_GEDA::connect_net(COMPONENT *newnet, int x0, int y0, int x1, int y1)
{
	// BUG: iterate nodes instead.
//	if(x0>x1){
//		std::swap(x0, x1);
//		std::swap(y0, y1);
//	}else{
//	}
	assert(newnet);
	trace5("LANG_GEDA::connect", newnet->long_label(), x0, y0, x1, y1);
	assert(x0!=x1 || y0!=y1); // hmm report error instead?
	CARD_LIST* scope;
	if (newnet->owner()) {
		scope = newnet->owner()->scope();
	}else{
		scope = newnet->scope();
	}

	std::string e0 = newnet->port_value(0);
	std::string e1 = newnet->port_value(1);

	// new endpoints may hit interior of an existing net
	for(CARD_LIST::const_iterator ci = scope->begin(); ci != scope->end(); ++ci) {
		if(DEV_NET* net=dynamic_cast<DEV_NET*>(*ci)){
			// connect end points of newnet hitting other nets
			if((*ci)->net_nodes()<2){
				// not necessary, as there is a place adjacent to the port.
				continue;
			}else if((net->port_value(0)+"AA").substr(0, INT_PREFIX.length()) != INT_PREFIX) { untested();
				// rail...?
				continue;
			}else if((net->port_value(1)+"AA").substr(0, INT_PREFIX.length()) != INT_PREFIX) {
				continue;
			}else{
			}

			std::string pv0 = net->port_value(0);
			std::string pv1 = net->port_value(1);
			// FIXME: faster...
#if 1
			NODE const* n1 = find_place(net, pv0);
			assert(n1);
			NODE const* n2 = find_place(net, pv1);
			int xn1 = get_x(n1);
			int yn1 = get_y(n1);
			int xn2 = get_x(n2);
			int yn2 = get_y(n2);
			assert(n2);
			if (xn1==xn2 && yn1==yn2) { untested();
				unreachable();
				// duplicate net. is this allowed in .sch?
				error(bDANGER,"singular net in %s, %s-%s\n", newnet->long_label().c_str(),
						pv0.c_str(), pv1.c_str());
			}else{
				connect_if_needed(x0, y0, net, e0);
				connect_if_needed(x1, y1, net, e1);
			}
#endif

		}else{
		}
	} // card loop
	
	
	assert(scope->nodes());
	// interior of new net new might hit existing "places"
	for(auto n : * scope->nodes()){
		if(n.first == "0"){ // BUG
			continue;
		}else if(!has_attributes(n.second->id_tag())) {
			// possibly rail
			trace1("no attrib.", n.first);
		}else{
			trace1("nodepos", n.first);
			int x = get_x(n.second);
			int y = get_y(n.second);
#if 1
			if(x0!=x1 && !in_order( x1, x, x0)){
			}else if(y0!=y1 && !in_order( y1, y, y0)){
			}else if( on_line(x, y, x0, y0, x1, y1)) {
					std::string value = n.first;
					int idx = newnet->net_nodes();
					newnet->set_port_by_index(idx, value);
					assert(idx == newnet->net_nodes()-1);
					_netnumber++;

			}else{
			}
#endif
		}
	}


	trace0("connect done");
}
/*--------------------------------------------------------------------------*/
// A net is in form N x0 y0 x1 y1 color
// Need to get x0 y0 ; x1 y1 ;
void LANG_GEDA::parse_net(CS& cmd, COMPONENT* x)
{
	trace1("parse_net", x->long_label());
	assert(x);
	// assert(lang_geda.find_type_in_string(cmd)=="net"); // no. at end of body...
	bool parse_net_body=0; // rearrange later!
	int coord[4];
	if(_netq.size()){
		netinfo n = _netq.front();
		coord[0] = n.x0;
		coord[1] = n.y0;
		coord[2] = n.x1;
		coord[3] = n.y1;
		store_attributes("geda_color="+::to_string(n.color), x->id_tag());
		_netq.pop();
		x->set_label("extranet" + ::to_string(_netnumber++));
	}else{
		// parse
		parse_net_body=1;
		if (cmd.fullstring().c_str()[0] != 'N'){ untested();
			throw Exception_CS("expecting net declaration", cmd);
		}
		unsigned here=cmd.cursor();
		// x0 y0 x1 y1 color
		std::string parsedvalue[5];
		int i=0;
		while (i<5) {
			if (cmd.is_alnum()){
				cmd>>" ">>parsedvalue[i];
				if(i!=4) coord[i] = atoi(parsedvalue[i].c_str());
			}else{untested();
				cmd.warn(bDANGER, here, x->long_label() +": Not correct format for net");
				return; // throw?
				break;
			}
			++i;
		}
		try{
			// is it a numeral??
			store_attributes("geda_color="+parsedvalue[4],  x->id_tag());
		}catch(Exception_No_Match const&){ untested();
			cmd.warn(bDANGER, here, x->long_label() +": color rejected");
		}
		x->set_label("net" + ::to_string(_netnumber++));
	}

	std::string attribs;
	std::string sep;
	assert(x->scope());
	assert(x->scope()->nodes());
	auto& nodes = *x->scope()->nodes();
	for(unsigned j=0; j<2; ++j){
		std::string portvalue;

		std::string netname = find_node_name(coord[0+2*j], coord[1+2*j], x->scope());
		if(netname=="") {
			std::string portname;
			portname = "nn_" + ::to_string(_nodenumber++);
			make_node_at( portinfo(portname, coord[0+2*j], coord[1+2*j]), x->scope());
			portvalue = std::string(INT_PREFIX) + portname; // node name.
																			//
		}else{
			portvalue = netname; // node name.
		}

#if 0
		if(netname == portvalue){ untested();
			trace4("match", netname, portvalue, coord[0+2*j], coord[1+2*j]);
		}else{ untested();
			incomplete();
			trace4("mismatch", netname, portvalue, coord[0+2*j], coord[1+2*j]);
			assert(0);
		}
#endif

		x->set_port_by_index(j, portvalue);
		std::string xs = to_string(coord[0+2*j]);
		std::string ys = to_string(coord[1+2*j]);
		std::string gattribs = "geda_x=" + xs + ", geda_y=" + ys;
		store_attributes(gattribs, nodes[portvalue]->id_tag());

		attribs += sep + "S0_x" + to_string(j+1) + "=" + xs 
		             + ", S0_y" + to_string(j+1) + "=" + ys ;
		sep = ", ";

	}

	trace2("store_attrib", x->short_label(), attribs);
	store_attributes(attribs, x->id_tag());
	trace2("stored attrib", x->short_label(), attributes(x->id_tag())->string(tag_t(x->id_tag())));


	if(x->short_label().substr(0, 8)=="extranet"){
		// HACK HACK HACK
	}else{
		connect_net(x, coord[0], coord[1], coord[2], coord[3]);
	}

	if(_netq.size()){
		//        unneccessary?
		trace1("queuing place", cmd.fullstring());
		cmd.reset();
	}else{
	}
	//To check if there are any attributes
	if(parse_net_body) {
	try {
		cmd.get_line("gnucap-geda>");
	}catch(Exception_End_Of_Input&){
		_gotline = false;
		return;
	}
	std::string paramvalue, paramname, dump;
	if(cmd.match1('{')){
		for (;;) {
			cmd.get_line("gnucap-geda-net>");
			if (cmd >> "}") {
				break;
			}else if(cmd>>"T"){
				cmd>>dump;
			}else{
				std::string paramname=cmd.ctos("=","",""),paramvalue;
				cmd >> "=" >> paramvalue;
				if (paramname=="netname" && paramvalue!="?"){
					x->set_label(paramvalue);
				}else{
					try{
						x->set_param_by_name(paramname, paramvalue);
					}catch(Exception_No_Match const&){
						store_attributes("geda_"+paramname+"=\""+paramvalue+"\"", x->id_tag());
					}catch(Exception_Clash const&){ untested();
						incomplete();
					}
				}
			}
		}
	} else {
		cmd.reset();
		_gotline = true;
		//OPT::language->new__instance(cmd,NULL,x->scope());
		return;
	}
	}
	cmd.reset();
	assert(!cmd.is_end()); // there could be a queue...
}
/*--------------------------------------------------------------------------*/
std::pair<int,int> componentposition(int* absxy, int* relxy, int angle, bool mirror);
/*--------------------------------------------------------------------------*/
const std::string LANG_GEDA::connect_place(CARD* card, int newx, int newy)
{
	assert(card);

	assert(card->scope());
	std::string netname = find_node_name(newx, newy, card->scope());
	std::string portvalue;
	if(netname=="") {
		std::string portname;
		portname = "cn_" + ::to_string(_nodenumber++);
		make_node_at( portinfo(portname, newx, newy), card->scope());
		portvalue = std::string(INT_PREFIX) + portname; // node name.
		connect_to_net(card, portname, newx, newy);
	}else{
		portvalue = netname; // node name.
	}

	return portvalue;
}
/*--------------------------------------------------------------------------*/
void LANG_GEDA::store_attributes(std::string attrib_string, tag_t x)
{
  assert(x);
  if(attrib_string!=""){
    set_attributes(x).add_to(attrib_string, x);
  }else{
  }
}
/*--------------------------------------------------------------------------*/
void LANG_GEDA::set_port_by_index(COMPONENT* c, int idx, std::string const& value,
		int x, int y)
{
	assert(c->scope());
	assert(c->scope()->nodes());
	auto& nodes = *c->scope()->nodes();

	std::string xs = to_string(x);
	std::string ys = to_string(y);
	std::string v = value; // BUG
	c->set_port_by_index(idx, v);
	//	store_node_pos(nodes[portvalue]->id_tag(), newx, newy);
	std::string gattribs = "geda_x=" + xs + ", geda_y=" + ys;
	store_attributes(gattribs, nodes[value]->id_tag());

	// S0_ here?
}
/*--------------------------------------------------------------------------*/
void LANG_GEDA::parse_component(CS& cmd, COMPONENT* x)
{
	// "component" means instance of a subckt
	trace4("LANG_GEDA::parse_component", x->long_label(), cmd.fullstring(),
			(x->owner()), (x->scope()));
	assert(x);
//	assert(_C);
	int c_x, c_y, angle;
	bool mirror;
	std::string dump,basename;
	std::string type=lang_geda.find_type_in_string(cmd);
	GEDA_SYMBOL* dev = _C;
	_C = NULL; // to make parse_symbol_file work
	if(dev->has_key("device")){
		assert(type==(*dev)["device"] || type==DUMMY_PREFIX+((*dev)["basename"]));
	}else{ untested();
	}
	std::string source("");

	// cannot read from cmd. too late.
	// cmd >> " " >> mirror >> " " >> basename;
	basename = (*dev)["basename"];
	c_x = dev->x;
	c_y = dev->y;
	angle = dev->angle();
	mirror = dev->mirror();
	trace1("LANG_GEDA::parse_component", basename);

	//To get port names and values from symbol?
	//Then set params below
	//Search for the file name
	// FIXME: should not need basename (dev instead)
	// FIXME: reads symbol file twice
	// std::vector<std::string*> coordinates=
	// parse_symbol_file(x,basename);
	// _symbol[basename];
	int newx, newy;

	try{
		// x->set_param_by_name("basename", basename);
		store_attributes("geda_basename=\""+basename+"\"",  x->id_tag());
	}catch(Exception_No_Match const&){untested();
	}catch(Exception_Clash const&){ untested();
		incomplete();
	}
	// set parameters

	for(GEDA_SYMBOL::const_iterator i=dev->begin(); i!=dev->end(); ++i) {
		if (i->first == "device"){
			x->set_dev_type( i->second );
		}else if ( i->first == "refdes" && i->second != "?" ){
			x->set_label(i->second);
			//                else if (paramname=="source")
			//                    source = paramvalue;
		}else if (i->first == "basename"){
			store_attributes("geda_basename=\""+basename+"\"",  x->id_tag());
		}else{
			try{
				trace2("p", i->first, i->second);
				x->set_param_by_name(i->first, i->second);
			}catch(Exception_No_Match const&){
			}catch(Exception_Clash const&){ untested();
				incomplete();
			}
		}
	}

	static unsigned instance;
	if(x->short_label()==""){
		if(dev->has_key("net")){
			// this might lead to trouble...
			x->set_label((*dev)["net"]);
		}else{
			x->set_label(basename + "_" + to_string(instance++));
		}
	}

	// connect ports
	int index = 0;
	index = 0;
	trace1("LANG_GEDA::parse_component setting ports", x->long_label());
	std::string attribs;
	std::string sep;
	assert(x->scope());
	assert(x->scope()->nodes());
	auto& nodes = *x->scope()->nodes();
	for (std::set<GEDA_PIN>::const_iterator i = dev->pinbegin(); i!=dev->pinend(); ++i ){
		int cc[2] = {c_x, c_y};
		int delta[2] = {-i->x0(), -i->y0()};

		std::pair<int,int> new_ = componentposition( cc, delta, angle, mirror );
		newx = new_.first;
		newy = new_.second;
		//delete (*i);
		//setting new place devices for each node searching for .
		//new__instance(cmd,NULL,Scope); //cmd : can create. Scope? how to get Scope? Yes!
		std::string /*const&*/ portvalue(connect_place(x, newx, newy));
		// port_by_name?!
		std::string xs = to_string(newx);
		std::string ys = to_string(newy);
		try{
			std::string p = i->label();
			trace4("LANG_GEDA::parse_component setting port0", x->long_label(), p, portvalue, (x));
			trace2("LANG_GEDA::parse_component setting port", i->label(), i->short_label());
			assert(i->label() == i->short_label());
			x->set_port_by_name(p, portvalue); // bug? nonconst portvalue...
		//	store_node_pos(nodes[portvalue]->id_tag(), newx, newy);
			std::string gattribs = "geda_x=" + xs + ", geda_y=" + ys;
			store_attributes(gattribs, nodes[portvalue]->id_tag());
			std::string portname = x->port_name(index);
			attribs += sep + "S0_x_" + p + "=" + xs
				          + ", S0_y_" + p + "=" + ys;
			sep = ", ";


		}catch(Exception_No_Match const&){
			try{
				trace2("LANG_GEDA::parse_component set port by index", i->pinseq(), portvalue);
				set_port_by_index(x, i->pinseq()-1, portvalue, newx, newy);

				attribs += sep + "S0_x" + to_string(i->pinseq()) + "=" + xs
								 + ", S0_y" + to_string(i->pinseq()) + "=" + ys;
				sep = ", ";
			}catch(Exception_Too_Many const&){ untested();
				// we have checked for pincount!
				unreachable();
			}
		}
		++index;
	}
	trace2("store_attrib", x->short_label(), attribs);
	store_attributes(attribs,  x->id_tag());
	if(source!=""){untested();
		trace1("parse_component", source);
	}
	delete dev;

	trace0("LANG_GEDA::parse_component done");
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
DEV_COMMENT* LANG_GEDA::parse_comment(CS& cmd, DEV_COMMENT* x)
{

	if(_C){
		unreachable();
		trace1("bug?",  (*_C)["basename"]);
		x->set("comment (incomplete) " + (*_C)["basename"]);
		delete _C;
		_C = NULL;
		return x;
	}else{
	}
	assert(x);
	trace2("LANG_GEDA::parse_comment", x->comment(), cmd.fullstring());
	x->set(cmd.fullstring());
	std::string dump, no_of_lines="";
	if (cmd >> "T "){
		_mode = mCOMMENT;
		for(int i=0; i<8; ++i){
			cmd >> dump >> " ";
		}
		cmd>>no_of_lines;
		if(no_of_lines==""){
			_no_of_lines = 1;
		}else{
			_no_of_lines = atoi(no_of_lines.c_str());
		}
	}else{
		if(_no_of_lines!=0){
			--_no_of_lines;
			if(_no_of_lines==0){
				_mode=mATTRIBUTE;
			}else{
			}
		}
	}
	return x;
	// trace1("LANG_GEDA::parse_comment done", x->comment());
}
/*--------------------------------------------------------------------------*/
DEV_DOT* LANG_GEDA::parse_symbol_file(DEV_DOT* x, const GEDA_SYMBOL& sym)const
{ untested();
	trace0("LANG_GEDA::parse_symbol_file");
	return x;
}
/*--------------------------------------------------------------------------*/
DEV_DOT* LANG_GEDA::parse_command(CS& cmd, DEV_DOT* x)
{
	std::string component_x, component_y, mirror, angle, dump, basename;
	//too late
	//    cmd >> "C" >> component_x >> " " >> component_y >> " " >> dump
	//        >> " " >> angle >> " " >> mirror >> " " >> basename;
	//too late...
	//           x->set(cmd.fullstring());
	assert(x);
	CARD_LIST* scope = (x->owner()) ? x->owner()->subckt() : &CARD_LIST::card_list;
	if(_C){
		error(bTRACE, "   >>" + cmd.fullstring() + "\n");
		x->set("gC");
		CS c(CS::_STRING, "gC");
		CMD::cmdproc(c, scope );
	} else {
		error(bTRACE, ">>>>>" + cmd.fullstring() + "\n");
		CMD::cmdproc(cmd, scope );
	}
	return 0;

	assert(_C);
	basename=(*_C)["basename"];
	trace3("LANG_GEDA::parse_command", x->s(), x->owner(), basename);

	bool graphical = 0;
	if(basename.length() > 4 && basename.substr(basename.length()-4) == ".sym"){ untested();
		//std::vector<std::string*> coord = parse_symbol_file( x, basename );
		incomplete();
		parse_symbol_file( x, *_C );
		graphical = !_C->ext_nodes();
	}else{ untested();
		cmd.reset();
		CMD::cmdproc(cmd, scope);
		delete x;
		return NULL;

	}
	trace3("LANG_GEDA::parse_command", basename, graphical, x->s());

	// bug: parse_symbol_file needs to tell us, if it is a command
	if( x->s() == "include"
			|| x->s() == "end"
			|| x->s() == "simulator"
			|| x->s() == "directive"
			|| x->s() == "gC"
			|| x->s() == "list" ){untested();

	}else{ untested();
		trace3("LANG_GEDA::parse_command not a command", x->s(), basename, graphical);
		cmd.reset();
		// for now, this is not a command
		// cmd.get_line(""); // bug? may fail and abort...

		if (!graphical){untested();
			trace2("LANG_GEDA::parse_command its a dev", x->s(), basename);
			// need command first to create devtype
			CMD::cmdproc(cmd, scope);
			_gotline = 1;
		}
		delete x;
		return NULL;

	}


	try{untested();
		cmd.get_line("");
		trace1("parse_command body?", cmd.fullstring());
	}catch(Exception_End_Of_Input&){untested();
	}

#if 0
	if (cmd.skip1("{")){untested();
		for(;;){untested();
			cmd.get_line("");
			if(cmd >> "}"){untested();
				break;
			}else if ( cmd >> "T" ){untested();
				cmd >> dump;
			}else{untested();
				std::string pname = cmd.ctos("=","","");
				std::string pvalue;
				cmd >> "=" >> pvalue;
				if(       pname=="pinlabel"){untested();
				}else if (pname=="pintype"){untested();
				}else if (pname=="file"){untested();
					x->set(x->s() + " " + pvalue);
				}
			}
		}
	}else{untested();
		_gotline = 1;
	}
#endif
	trace1("LANG_GEDA::parse_command instance done", x->s());

	if(0){ // now?
		CMD::cmdproc(cmd, scope);
		delete x;
		return NULL;
	} else { untested();
		return x;
	}
}
/*--------------------------------------------------------------------------*/
MODEL_CARD* LANG_GEDA::parse_paramset(CS& cmd, MODEL_CARD* x)
{ untested();
	assert(x);
	return NULL;
}
/*--------------------------------------------------------------------------*/
CARD_LIST::const_iterator
LANG_GEDA::find_nondevice(std::string name, CARD_LIST* Scope)
{
	return find_card(name, Scope, true);
}
/*--------------------------------------------------------------------------*/
CARD_LIST::const_iterator LANG_GEDA::find_card(
		std::string name, CARD_LIST* Scope, bool nondevice)
{
	if (!Scope) Scope = &CARD_LIST::card_list;
	CARD_LIST::const_iterator i = Scope->find_(name);
	if(nondevice){
		while (i!=Scope->end()) {
			if((*i)->is_device()){ untested();
				trace1("skip", (*i)->long_label());
				i = Scope->find_again(name, ++i); // skip
			} else {
				break;
			}
		}
	}
	if (i == Scope->end()) {
		throw Exception_Cant_Find(name, "scope");
	}
	return i;
}
/*--------------------------------------------------------------------------*/
MODEL_SUBCKT* LANG_GEDA::parse_module(CS& cmd, MODEL_SUBCKT* x)
{
	CARD_LIST* scope = x->owner()?x->owner()->scope():x->scope();

	int c_x=0;
	int c_y=0;
	bool mirror = false;
	angle_t angle=a_invalid;
	std::string basename;
	if (!_C) { untested();
		// parse from cmd...
		incomplete(); // not possible right now
		//cmd>>"C";
		//cmd>>component_x>>" ">>component_y>>" ">>dump>>" ">>angle>>" ">>mirror>>" ">>basename;
	}else{
		if ( !_C->has_key("source") && !_C->has_key("file") ){
			return 0;
		}
		*_C >> x;
		c_x = _C->x;
		c_y = _C->y;
		mirror = _C->mirror();
		angle = _C->angle();
		basename = (*_C)["basename"];
		USE(c_x);
		USE(c_y);
		USE(mirror);
		USE(angle);
		USE(basename);
	}

	x->set_label((*_C)["device"]);
	if(_C->has_key("source")){
		GEDA_SYMBOL* tmp=_C;
		_C = NULL;
		if(MODEL_GEDA_SUBCKT* X = dynamic_cast<MODEL_GEDA_SUBCKT*>(x)){
			trace1("found a source thing", _defconn);
			X->set_defconn(_defconn);
		}
		read_file( (*tmp)["source"], scope, x);
		_C = tmp;
	}else if(_C->has_key("file") ){
		trace1("spice-sdb hack", (*_C)["file"] );
		// file must be a spice deck defining device.
		// just source it and check...
		// then rewire
		read_spice((*_C)["file"], scope, x);
		const CARD* modelcard;
		try{
			modelcard = *find_nondevice( (*_C)["device"], x->subckt());
		} catch(Exception_Cant_Find const&){ untested();
			error(bDANGER,"spice-sdb compat: no %s in %s\n",
					(*_C)["device"].c_str(), (*_C)["file"].c_str());
			modelcard = NULL;
		}

		if (modelcard) {
			COMPONENT* a=prechecked_cast<COMPONENT*>(modelcard->clone_instance());
			assert(a);
			CMD::command("options lang=spice", scope); // still case problems...
			a->set_label("X"+(*_C)["device"]);
			a->set_dev_type((*_C)["device"]);
			a->set_owner(x);

			CMD::command("options lang=gschem", scope);
			(*_C) >> a;
		//	store_attributes("AA", a->id_tag());
			scope->push_back(a);
		}
	}

	trace5("LANG_GEDA::parse_module", c_x, c_y, mirror, angle, basename);
	return x;
}
/*--------------------------------------------------------------------------*/
// is this really necessary?
COMPONENT* LANG_GEDA::parse_componmod(CS& cmd, COMPONENT* x)
{
	error(bTRACE, ">pcm>" + cmd.fullstring() + "\n");
	assert(x);
	assert(_C);
	cmd.reset();
	std::string type=find_type_in_string(cmd);
	trace1("LANG_GEDA::parse_componmod", type);

	// assert(type=="C"); BUG
	// too late
	// cmd>>"C";
	// cmd>>component_x>>" ">>component_y>>" ">>dump>>" ">>angle>>" ">>mirror>>" ">>basename;
	if(!_C->ext_nodes()) return 0;
	int c_x = _C->x;
	int c_y = _C->y;
	bool mirror = _C->mirror();
	angle_t angle = _C->angle();
	std::string basename = (*_C)["basename"];
	trace5("LANG_GEDA::parse_componmod", c_x, c_y, mirror, angle, basename);

	//open the basename to get the ports and their placements
	//parse_ports(newcmd,x);

	//
	assert(_C->has_key("device"));
	x->set_label(DUMMY_PREFIX+basename);
	// x->set_label((*_C)["device"]);

	// std::vector<std::string*> coord=parse_symbol_file(x,basename);
	MODEL_SUBCKT* m = dynamic_cast<MODEL_SUBCKT*>(x);
	assert(m);
	*_C >> m;
	// move?
	try{
		x->set_param_by_name("x", to_string(c_x));
		x->set_param_by_name("y", to_string(c_y));
		x->set_param_by_name("mirror", to_string(mirror));
		x->set_param_by_name("angle", to_string(angle));
	}catch(Exception_No_Match const&){ untested();
	}

	cmd.reset();
	/*type = "graphical";
	  x->set_dev_type(type);
	  std::cout<<x->dev_type()<<" is the dev type\n";
	  */
	// delete _C; // no. keep _C, no instance yet.
	// _C = 0;
	return x;
}
/*--------------------------------------------------------------------------*/
COMPONENT* LANG_GEDA::parse_instance(CS& cmd, COMPONENT* x)
{
	cmd.reset();
	parse_type(cmd, x); //parse type will parse the component type and set_dev_type
	error(bTRACE, ">inst>" + cmd.fullstring() + "\n");
	if (x->dev_type()=="net"){
		parse_net(cmd,x);
//	}else if(x->dev_type()=="place"){
//		parse_place(cmd,x);
	}else {
		parse_component(cmd,x);
	}
	//No warnings needed.
	//cmd.check(bWARNING, "what's ins this?");
	return x;
}
/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
static void trim_tail(std::string& s)
{
	size_t size=s.size();
	if(size==0){ untested();
	}else if(s[size-1]==' '){
		s.resize(size-1);
	}else{ untested();
	}
}
/*----------------------------------------------------------------------*/
// this is a hack finding the std::string in a C block.
// no parse, if _C is present.
GEDA_SYMBOL* LANG_GEDA::parse_C(CS& cmd)const
{
	trace2("LANG_GEDA::parse_C", cmd.fullstring(), cmd.tail());
	if (_C){
		return _C;
	}else{
	}

	int c_x, c_y, c_a;
	bool c_m, c_sel;
	std::string basename;
	c_x = cmd.ctoi();
	c_y = cmd.ctoi();
	c_sel = cmd.ctob(); USE(c_sel);
	c_a = cmd.ctoi();
	c_m = cmd.ctob();
	cmd >> " " >> basename;
	trace1("more", basename);
	assert( ! ( c_a % 90 ) );
	assert( -1 < c_a && c_a < 271 );
	trace1("more1", basename);
	GEDA_SYMBOL const* sym;
	try{
		sym = find_symbol(basename);
	}catch(...){ untested();
		cmd.warn(0, 0, "cannot find " + basename);
		throw;
	}
	trace1("more1b", basename);
	_C = sym->clone();
	GEDA_SYMBOL& D = *_C;
	trace1("more2", basename);
	D.x = c_x;
	D.y = c_y;
	D.set_angle(angle_t(c_a));
	D.set_mirror(c_m);
	std::string& s = (*_C)["basename"];
	trace2("something", s, basename);
	s = basename; // hmmm...
	try{
		cmd.get_line("gnucap-geda-"+basename+">");
		trace1("parse_C body?", cmd.fullstring());
		if(cmd >> '{') {
			for (;;) {
				cmd.get_line("gnucap-geda-"+basename+">");
				if (cmd >> "}") {
					cmd.reset();
					break;
				} else if(cmd >> "T") {
				} else {
					std::string name = cmd.ctos("=","","");
					cmd >> "=";
					std::string value = cmd.tail();
					trim_tail(value); // BUG?! where does the space come from?
					(*_C)[name] = value;
				}
			}
		} else {
			trace2("C w/o body", cmd.fullstring(), (*_C)["basename"]);
			_gotline = 1; // dont read another time.
		}
	}catch(Exception_End_Of_Input&){
		trace1("something went wrong", basename);
		_gotline = 0; // try again in main loop (and fail)
	}
	assert(_C);
	return _C;
}
/*----------------------------------------------------------------------*/
/* find_type_in_string : specific to each language which gives the type of the
 * cmd Eg: verilog : comment ,resistor etc
 * In Gschem the broad high level types are
 * The following don't mean anything to the electrical part of the circuit and
 * they are of type graphical
 *  version  v    * circle   V
 *  line     L    * arc      A
 *  picture  G    * path     H
 *  box      B    * text     T (not attributes)
 * The following have electrical meaning and are of type
 *  net          N
 *  bus          U
 *  pin          P
 *  component    C
 *  attributes   T (enclosed in {})  // don't include in find_type_in_string
 * Will check for type graphical (type=dev_comment) else type will be
 * net or bus or pin or component\
 */
std::string LANG_GEDA::find_type_in_string(CS& cmd)const
{
	trace4("LANG_GEDA::find_type_in_string", cmd.tail(), (_C), _netq.size(), _mode);
	size_t here = cmd.cursor(); //store cursor position to reset back later
	bool reset=true;
	std::string type;   //stores type : should check device attribute..
	//graphical=["v","L","G","B","V","A","H","T"]
	//
	//
	if (_mode==mCOMMENT){
		type = "dev_comment";
		reset = false;
	} else if (_netq.size()){
		assert(!_C);
		type = "net";
		reset = false;
	} else if (_C || cmd >> "C "){
		trace2("find_type_in_string C", cmd.fullstring(), _gotline);
		const GEDA_SYMBOL* D = parse_C(cmd);
		assert(_C);
		trace3("find_type_in_string C", (_C), (*D)["device"], (*D)["basename"]);

		if (D->ext_nodes()){
			// nets and devices.. see below
		}else if (!D->has_key("device") ){
			trace2("have no pins", _C->ext_nodes(), (*D)["basename"]);
//			delete _C;
//			_C = 0;
			return "dev_comment";
		}else{
			if ((*_C)["device"] == "directive"){
				incomplete();
				return "dev_comment";
			}else{
				return "dev_comment";
			}
		}
		trace1("have pins", _C->ext_nodes());

		if (D->has_key("device") && (*D)["device"]!=""){
			std::string dev=(*D)["device"];
			error(bTRACE, ">dev>" + dev + "\n");
			const CARD* modelcard;
			try {
				modelcard = *find_nondevice(dev);
				trace1("found nondevice", dev);
			} catch (Exception_Cant_Find const&){
				modelcard = NULL;
				trace1("no nondevice", dev);
			}
			if (CARD* c = device_dispatcher[dev]){
				COMPONENT* d = prechecked_cast<COMPONENT*>(c);
				if ( d->max_nodes() >= (*D).ext_nodes()
						&& d->min_nodes() <= (*D).ext_nodes()){
					type = dev;
				}
			}else if (modelcard) {
				if (const COMPONENT* d = prechecked_cast<const COMPONENT*>(modelcard)){
					if(d->max_nodes() >= D->ext_nodes()
							&& d->min_nodes() <= D->ext_nodes()){
						type = dev;
					}
				}else if(const MODEL_SUBCKT* d=
						prechecked_cast<const MODEL_SUBCKT*>(modelcard)){ untested();
					/// eek
					if(int(d->max_nodes()) >= D->ext_nodes()
							&& d->min_nodes() <= D->ext_nodes()){ untested();
						type = dev;
					}
				}else{
					cmd.warn(bDANGER, dev + " Nothing there?");
					type = dev;
				}
			}else{
				std::string modulename = DUMMY_PREFIX + (*D)["basename"];
				trace1("symbolthere?", modulename);
				CARD_LIST::const_iterator i = CARD_LIST::card_list.find_(modulename);
				if(i != CARD_LIST::card_list.end()) {
					trace1("instanceofsymbol", (*D)["basename"]);
					type = modulename;
				} else {
					trace0("C -- new subckt?");
					type = "gC";
				}
			}
		} else if (D->has_key("net")) {
			trace2("found rail", (*D)["net"], cmd.fullstring() );
			if (CARD* c = device_dispatcher["rail"]){
				COMPONENT* d = prechecked_cast<COMPONENT*>(c);
				if(d->max_nodes() >= D->ext_nodes()
						&& d->min_nodes() <= D->ext_nodes()){
					type = "rail";
					(*_C)["device"] = type;
				}
			}
		} else { untested();
			type = "gC"; // declare module first...
		}
		trace1("find_type_in_string, no reset", type);
		reset = false;
	} else if (cmd >> "v "){
		reset = false;
		type = "dev_comment";
	} else if (cmd >> "L "){
		reset = false;
		type = "dev_comment";
	} else if (cmd >> "G "){ untested();
		reset = false;
		type = "dev_comment";
	} else if (cmd >> "B "){
		reset = false;
		type = "dev_comment";
	} else if (cmd >> "V "){ untested();
		reset = false;
		type = "dev_comment";
	} else if (cmd >> "A "){
		reset = false;
		type = "dev_comment";
	} else if (cmd >> "H "){ untested();
		reset = false;
		type = "dev_comment";
	} else if (cmd >> "T "){
		reset = true;
		type = "dev_comment";
	} else if (cmd >> "}"){
		try {
			cmd.get_line("brace-bug>");
			return find_type_in_string(cmd);
		} catch(Exception_End_Of_Input&){
			return "dev_comment";
		}
	}else if(cmd >> "N "){
		return "net";
	}else if(cmd >> "U "){ untested();
		type = "bus";
		reset = true;
	}else if(cmd >> "P "){ untested();
		type = "pin";
		reset = true;
	}else if(cmd >> "place "){untested();
		// hmmm. ouch
		type = "place";
		reset = true;
	}else{
		switch(_mode){
			case mCOMMENT: return "dev_comment";
			default : cmd >> type;
		}
	}
	trace3("LANG_GEDA::find_type_in_string done", type, reset, _gotline);
	//Not matched with the type. What now?
	//trace2("find_type_in_string", cmd.fullstring(),type);
	if(reset){
		cmd.reset(here);
	}else{
	}
	return type;    // returns the type of the std::string
}
/*----------------------------------------------------------------------*/
/* parse_top_item :
 * The top default thing that is parsed. Here new__instances are
 * created and (TODO)post processing of nets is done
 */
void LANG_GEDA::parse_top_item(CS& cmd, CARD_LIST* Scope)
{
	parse_item_(cmd, NULL, Scope);
}
/*----------------------------------------------------------------------*/
// check: LANGUAGE::parse_item...
void LANG_GEDA::parse_item_(CS& cmd, CARD* owner, CARD_LIST* scope)const
{
	error(bTRACE, ">pi>" + cmd.fullstring() + "\n");

	// .... _gotline means:
	// - component needs to be instanciated after sckt declaration.
	// - parser found nonbrace when trying to parse body
	trace3("LANG_GEDA::parse_item_", _gotline, _netq.size(), (_C));
	if(_C && !_gotline){ untested();
		_C = NULL;
		throw(Exception_CS("something wrong", cmd));
	}else if(_C){
	}else if(!_gotline && !_netq.size()){
		cmd.get_line("gnucap-geda>");
	}else if(!_netq.size()){
		_gotline = 0;
	}else{
	}

	//problem: if new__instance interprets as command, Scope is lost.
	trace2("LANG_GEDA::parse_item_", cmd.fullstring(), cmd.tail());
	CARD_LIST* s;
	if (owner){
	  s = owner->subckt();
	}else{
	  s =	scope;
	}
	assert(!cmd.match1("{"));

	// nothing if cmd.is_end...
	error(bTRACE, ">pi2>" + cmd.fullstring() + "\n");
	lang_geda.new__instance(cmd, dynamic_cast<MODEL_SUBCKT*>(owner), s);
} // LANG_GEDA::parse_item_
/*----------------------------------------------------------------------*/
// Code for Printing schematic follows
/*----------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
#if 0 // not yet
static void print_type(OMSTREAM& o, const COMPONENT* x)
{untested();
	assert(x);
	o << x->dev_type();
}
/*--------------------------------------------------------------------------*/
static void print_label(OMSTREAM& o, const COMPONENT* x)
{untested();
	assert(x);
	o << x->short_label();
}
#endif
/*--------------------------------------------------------------------------*/
void LANG_GEDA::print_node_xy(OMSTREAM& o, const COMPONENT* c, int portindex)
{
	std::string nodename = c->port_value(portindex);
	const CARD_LIST* scope = c->owner()?c->owner()->scope():c->scope();
	assert(scope);
	assert(scope->nodes());

	NODE const* n = (*scope->nodes())[nodename];
	find_place_(c, nodename);

	if(has_attributes(n->id_tag())){
		int x = get_x(n);
		int y = get_y(n);
		o << x << " " << y << " ";
	}else{ untested();
		unreachable();
		o << "???? ???? ";
	}
}
/*--------------------------------------------------------------------------*/
std::pair<int,int> componentposition(int* absxy, int* delxy, int angle, bool mirror)
{
	int newx = absxy[0];
	int newy = absxy[1];
	if(!mirror){
		switch(angle){
			case 0:
				newx -= delxy[0]; // -1  0
				newy -= delxy[1]; //  0 -1
				break;
			case 90:
				newx += delxy[1]; //  0  1
				newy -= delxy[0]; // -1  0
				break;
			case 180:
				newx += delxy[0]; //  1  0
				newy += delxy[1]; //  0  1
				break;
			case 270:
				newx -= delxy[1]; //  0 -1
				newy += delxy[0]; //  1  0
				break;
		}
	}else{
		switch(angle){
			case 0:
				newx += delxy[0]; //  1  0
				newy -= delxy[1]; //  0 -1
				break;
			case 90:
				newx += delxy[1]; //  0  1
				newy += delxy[0]; //  1  0
				break;
			case 180:
				newx -= delxy[0]; // -1  0
				newy += delxy[1]; //  0  1
				break;
			case 270:
				newx -= delxy[1]; //  0 -1
				newy -= delxy[0]; // -1  0
				break;
		}
	}
	return std::pair<int,int>(newx,newy);
}
/*--------------------------------------------------------------------------*/
// urghs
static std::string componentposition_string(int* absxy, int* relxy, int angle, bool mirror)
{
	return to_string(componentposition(absxy, relxy, angle, mirror).first)
		+ " " +
		to_string(componentposition(absxy, relxy, angle, mirror).second);
}
/*--------------------------------------------------------------------------*/
NODE const* LANG_GEDA::find_place(const CARD* x, std::string name)
{
	const CARD_LIST* scope = x->owner()?x->owner()->scope():x->scope();
	assert(scope->nodes());

	NODE const* n = (*scope->nodes())[name];
	assert(n);

	store_node_position(n, scope);

	return n;
}
/*--------------------------------------------------------------------------*/
std::pair<int, int> LANG_GEDA::find_place_(const CARD* x, std::string name)
{
	NODE const* p = find_place(x, name);
	assert(p);
	std::pair<int, int> a;
	a.first = get_x(p);
	a.second = get_y(p);
	trace3("LANG_GEDA::find_place_", name, a.first, a.second);
	return a;
}
/*--------------------------------------------------------------------------*/
std::string* LANG_GEDA::find_place_string(const CARD* x, std::string name)
{
	std::string* a = new std::string[2];
	try{
		std::pair<int,int> b = find_place_(x, name);
		a[0] = to_string(b.first);
		a[1] = to_string(b.second);
		return a;
	}catch(Exception_Cant_Find const&){ untested();
		trace1("no place string", name);
		return NULL;
	}
}
/*--------------------------------------------------------------------------*/
void LANG_GEDA::print_net(OMSTREAM& o, const COMPONENT* c)
{
	assert(c);
	assert(c->dev_type()=="net");
	o << "N ";
	//print_coordinates(node); //Will search through the CARD_LIST to find the node
	//o<< node0x << node1x
	//o<< node1x << node2x
	print_node_xy(o, c, 0);
	print_node_xy(o, c, 1);
	// if(x->value().string()=="NA( 0.)"){ untested();
	// 	o << "4\n"; // HACK
	// }else if(x->value().string()!=""){untested();
	// 	o  << x->value().string()<<"\n"; //The color
	// }else
	{
		o << "4\n";
	}
}
/*--------------------------------------------------------------------------*/
/* C x y selectable angle mirror basename
 * {untested();
 *  <params>
 * }
 */
void LANG_GEDA::print_component(OMSTREAM& o, const COMPONENT* x)
{
	assert(x);
	std::string _angle,_mirror;
	o << "C ";

	std::string basename; //  = x->param_value(0); // (x->param_count()-1);
	if(has_attributes(x->id_tag())) {
		basename = attributes(x->id_tag())->operator[]("geda_basename");
	}else{ untested();
		unreachable();
	}
	GEDA_SYMBOL* sym = _symbol[basename];
//	for( std::set<GEDA_PIN>::const_iterator p = sym->pinbegin();
//			p!=sym->pinend(); ++p){ untested();
//		trace3("LANG_GEDA::print_component", p->label(), p->x0(), p->y0());
//	}
	// unsigned howmany = sym->ext_nodes();
	unsigned howmany = x->net_nodes();

	std::vector<const std::pair<int,int>*> coordinates;
	coordinates.resize(howmany);
	std::vector<std::string*> abscoord;
	for(unsigned ii=0; ii<howmany; ++ii){
		std::string n=x->port_name(ii);
		std::string val=x->port_value(ii);
		abscoord.push_back(find_place_string(x, val));
	//	trace4("LANG_GEDA::print_component", n, val, abscoord.back()[0], abscoord.back()[1]);
		if(GEDA_PIN const* P = sym->pin(n)){
			trace1("LANG_GEDA::print_component", P->label());
			coordinates[ii] = &P->X();
		}else if(GEDA_PIN const* P = sym->pin(ii+1)){
			// label mismatch. trying seq.
			coordinates[ii] = &P->X();
		}else{ incomplete();
			trace2("LANG_GEDA::print_component", ii, sym->pin(ii));
			throw(Exception_Cant_Find("pin in " + basename, n));
		}
	}
	static std::string angle[4]={"0","90","180","270"};
	static std::string ms[2] = {"0","1"};
	std::string xy="";
	bool gottheanglemirror = false; // guessed rotation matches ports.
	for(int ii=0; ii<4 ; ++ii){
		if(gottheanglemirror){
			break;
		}

		{
			_angle = angle[ii];
			for(unsigned mir=0; mir<2; ++mir) {
				trace2("print_comp case", mir, angle[ii]);
				_mirror = ms[mir];
				xy="";
				for(unsigned pinind=0; pinind<howmany; ++pinind){
					int a[2];
					int c[2];
					assert(pinind < abscoord.size());
					assert(abscoord[pinind]);
					a[0] = atoi(abscoord[pinind][0].c_str());
					a[1] = atoi(abscoord[pinind][1].c_str());
					assert(pinind < coordinates.size());
					assert(coordinates[pinind]);
					c[0] = coordinates[pinind]->first;
					c[1] = coordinates[pinind]->second;
					std::string pos = componentposition_string(a, c, 90*ii, mir);
					if (pinind==0){
						// first port. guess component position.
						xy = pos;
						gottheanglemirror = true;
						trace2("print_comp guess from 1st port", ii, pos);
					}else if(xy != pos){
						trace2("print_comp check other port", ii, pos);
						trace2("print_comp rejecting", mir, _angle);
						// check if it is consistent with the other ports.
						gottheanglemirror = false;
						break;
					}else{
						trace2("print_comp match", pinind, pos);
					}
				}
				if (gottheanglemirror) {
					break;
				}
			}
		}
	}
	if(!gottheanglemirror){ unreachable();
		// could not match symbol pin to places (!?)
	}else{
	}
	o << xy << " " << "1" << " " << _angle << " "
	  << _mirror << " " << basename<< "\n";
	//map those with the absolute positions of nodes and place the device
	//such that it is in between the nodes.
	//std::vector<std::string*> coord = parse_symbol_file(static_cast<COMPONENT*>(x) , basename);

	//Got the x and y
	//To print angle mirror etc, to get from the intelligent positioning

	//*To check if there are any attributes at all first
	//    If not return;
	bool _parameters=false;
	bool _label=false;
	bool _devtype=false;
	if(x->short_label()!=""){
		_label=true;
	}
	if(x->param_count()>6){
		_parameters=true;
	}
	if(x->dev_type()!="" && x->dev_type().substr(0,DUMMY_PREFIX.length())!=DUMMY_PREFIX){
		_devtype=true;
	}
	if (_label or _parameters or _devtype){
		o << "{\n";
		if(_devtype){
			o << "T "<< xy << " 5 10 0 1 0 0 1\n";
			o << "device=" << x->dev_type() << "\n";
		}
		if(_label){
			o << "T "<< xy << " 5 10 0 1 0 0 1\n";
			o << "refdes=" << x->short_label() << "\n";
		}
		if(_parameters){
			for(int i=0; i < x->param_count(); ++i){
				if(x->param_value(i)=="NA( 0.)"){ untested();
				}else if(x->param_value(i)=="NA( NA)"){ untested();
				}else if(x->param_value(i)=="NA( 27.)"){untested();
				}else if(x->param_name(i)=="basename"){ untested();
				}else if(!x->param_is_printable(i)){ untested();
				}else{
					o << "T "<< xy << " 5 10 0 1 0 0 1\n";
					o << x->param_name(i) << "=" << x->param_value(i) << "\n";
				}
			}
		}
		o << "}\n";
	}
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
void LANG_GEDA::print_paramset(OMSTREAM& o, const MODEL_CARD* x)
{
	trace1("LANG_GEDA::print_paramset", x->short_label());
	if(!x->component_proto()){ untested();
	}else if(x->short_label() != x->component_proto()->short_label()){ untested();
	}else if(auto s = dynamic_cast<BASE_SUBCKT const*>(x->component_proto())){
		print_module(o, s);
	}else{ untested();
		incomplete();
	}
}
/*--------------------------------------------------------------------------*/
void LANG_GEDA::print_module(OMSTREAM& o, const MODEL_SUBCKT* x)
{
	assert(x);
	trace3("LANG_GEDA::print_module", x->short_label(), x->id_tag(), has_attributes(x->id_tag()));
	//o<<x->short_label();
	//o<<"\n";
	assert(x->subckt());
	if(x->short_label().find(DUMMY_PREFIX)!=std::string::npos){ untested();
		trace0("Got a placeholding model");
	}else{
     // a bit of a hack.
		for (CARD_LIST::const_iterator
		     ci = x->subckt()->begin(); ci != x->subckt()->end(); ++ci) {
			print_item(o, *ci);
		}
	}
}
/*--------------------------------------------------------------------------*/
void LANG_GEDA::print_instance(OMSTREAM& o, const COMPONENT* x)
{
	trace2("LANG_GEDA::print_instance", has_attributes(x->id_tag()), x->long_label());
	// print_type(o, x);
	// print_label(o, x);
	if(x->dev_type()=="net"){
		print_net(o, x);
	}else if(x->dev_type()=="place"){
	}else{
		//Component
		print_component(o ,x);
	}
}
/*--------------------------------------------------------------------------*/
void LANG_GEDA::print_comment(OMSTREAM& o, const DEV_COMMENT* x)
{
	assert(x);
	o << x->comment() << '\n';
}
/*--------------------------------------------------------------------------*/
void LANG_GEDA::print_command(OMSTREAM& o, const DEV_DOT* x)
{untested();
	assert(x);
	o << x->s() << '\n';
}
/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
class CMD_GEDA : public CMD {
	public:
		void do_it(CS& cmd, CARD_LIST* Scope)override {
			LANGUAGE* oldlang = OPT::language;
			// BUG breaks direct "options lang=gschem", does it?
			lang_geda._mode = lang_geda.mATTRIBUTE;
			lang_geda._no_of_lines = 0;
			lang_geda._gotline = false;
			//
			std::string filename;
			cmd >> filename;
			trace1("gschem", filename);
			if(filename==""){untested();
				command("options lang=gschem", Scope);
				return;
			}
			bool module=false;
			bool symbol=false;
			std::string device="";
			std::string source="";
			std::string defconn="";
			trace1("args", cmd.tail());
			size_t here = cmd.cursor();
			do{
				ONE_OF
					|| Get(cmd, "module", &module)
					|| Get(cmd, "symbol", &symbol)
					|| (cmd.umatch("defconn|default_connect {=}") &&
							(ONE_OF
							 || Set(cmd, "o{pen}",        &defconn, std::string("open"))
							 || Set(cmd, "g{nd}",         &defconn, std::string("gnd"))
							 || Set(cmd, "a{uto}",        &defconn, std::string("auto"))
							 || Set(cmd, "p{romiscuous}", &defconn, std::string("promisc"))
							 || cmd.warn(bWARNING, "need open, gnd, auto")
							))
					|| (cmd.umatch("device {=}") && ( cmd >> device ) )
					|| (cmd.umatch("source {=}") && ( cmd >> source ) ) ;
			} while (cmd.more() && !cmd.stuck(&here));
			cmd.check(bWARNING, "what's this?");

			MODEL_GEDA_SUBCKT* model=NULL;

			// hack
			lang_geda._defconn = defconn;

			// hmm could take from "subckt" comment block?
			std::string label = (device=="")? filename : device;

			if(symbol){
				// BUG: deduplicate
				//GEDA_SYMBOL* sym = lang_geda._symbol[filename]->clone();
				GEDA_SYMBOL* sym = make_GEDA_SYMBOL(filename);

				trace3("symbol", sym->ext_nodes(), filename, lang_geda._symbol[filename]->ext_nodes());
				assert(sym->ext_nodes());
				if(source!=""){
					(*sym)["source"] = source;
				}else{
					(*sym)["source"] = filename;
				}
				if((*sym)["source"]==""){untested();
					OPT::language = oldlang;
					throw Exception_CS("empty source", cmd);
				}else{
				}
#if 0 // does not make sense...
				model = new MODEL_GEDA_SUBCKT(); // BUG: ask dispatcher?
				*sym >> model;
				delete sym;
				model->set_label(label);
				try{ untested();
					LANG_GEDA::read_file(source, Scope, model);
				}catch(...){ untested();
					delete (MODEL_GEDA_SUBCKT*) model;
					throw;
				}
				Scope->push_back(model);
#else
				//align(model); // might be needed for gnucap .36
				//sym->set_dev_type(filename);
				sym->set_label(label);
				sym->set_owner(nullptr);
				Scope->push_back(sym);
#endif
			}else if(module) {
				// BUG: deduplicate
				trace2("reading module", filename, label);
				model = new MODEL_GEDA_SUBCKT(); // BUG: ask dispatcher?
				model->set_label(label);
				model->set_owner(nullptr);
				trace1("new MGS", defconn);
				model->set_defconn(defconn);
				try{
					LANG_GEDA::read_file(filename, Scope, model);
				}catch(...){
					assert(0);
					delete (MODEL_GEDA_SUBCKT*) model;
					throw;
				}
				trace3("done reading module", filename, model->long_label(), model->int_nodes());
				trace1("...", model->subckt()->nodes()->how_many());
				//align(model); // might be needed for gnucap .36
// 				for(unsigned i=0; i<(unsigned)model->net_nodes(); ++i){
// 					trace3("", i, model->port_value(i), model->n_(i).e_());
// 				}
				Scope->push_back(model);
			} else if(filename!=""){
				// BUG: deduplicate
				command("options lang=gschem", Scope);
				LANG_GEDA::read_file(filename, Scope);
			} else {untested();
				unreachable();
			}
			trace1("done", oldlang);
			// command("options lang="+oldlang, Scope);
			OPT::language = oldlang;
		}
		// void align(MODEL_SUBCKT* s) const;
} p8;
DISPATCHER<CMD>::INSTALL
// FIXME (hoW?): v conflicts with spice vsource
d8(&command_dispatcher, "geda|v ", &p8);
/*----------------------------------------------------------------------*/
// fixme.
void LANG_GEDA::read_spice(std::string f, CARD_LIST* Scope, MODEL_SUBCKT* owner)
{
	CS cmd(CS::_INC_FILE, f);
	CMD::command("options lang=spice", Scope);

	try{
		for(;;){
			cmd.get_line("spice-sdb>");
			OPT::language->new__instance(cmd, owner, Scope);
		}

	}catch (Exception_End_Of_Input& e){
	}
	CMD::command("options lang=gschem", Scope);
}
/*----------------------------------------------------------------------*/
void LANG_GEDA::read_file(std::string f, CARD_LIST* Scope, MODEL_SUBCKT* model)
{
	error(bDEBUG, "reading file "+f+"\n");
	CS cmd(CS::_INC_FILE, f);

	/// gnucap-uf bug: getline uses OPT::language
	LANGUAGE* oldlang = OPT::language;
	OPT::language = &lang_geda;
	///

	try{
		for(;;){
			// new__instance. but _gotline hack
			lang_geda.parse_item_(cmd, model, Scope);
		}
	}catch (Exception_CS&){ untested();
		/// gnucap-uf bug
		OPT::language = oldlang;
		///
		throw;
	}catch (Exception_End_Of_Input& e){
	}
	/// gnucap-uf bug
	OPT::language = oldlang;
	///
}
/*----------------------------------------------------------------------*/
class CMD_C : public CMD {
	void do_it(CS& cmd, CARD_LIST* Scope)override {
		trace1("CMD_C::do_it", (Scope));
		CARD* c = device_dispatcher["symbol"]; // future overrides?
		if(!c) c = device_dispatcher["subckt"];
		assert(c);
		CARD* clone = c->clone();
		COMPONENT* new_compon = prechecked_cast<COMPONENT*>(clone);

		// hmm hack
		if(MODEL_GEDA_SUBCKT* X = dynamic_cast<MODEL_GEDA_SUBCKT*>(new_compon)){
			X->set_defconn(lang_geda._defconn);
		}

		assert(new_compon);
		assert(new_compon->subckt());
		assert(new_compon->subckt()->is_empty());
		// BUG?: new_compon doesnt know its scope!
		// thats okay, symbols are global anyway.
		new_compon->set_owner(nullptr); // ?
		if (lang_geda.parse_module(cmd, dynamic_cast<MODEL_SUBCKT*>(new_compon))) {

			// Scope->push_back(new_compon);
			CARD_LIST::card_list.push_back(new_compon);
			lang_geda._gotline = true;
			cmd.reset();
		} else if (lang_geda.parse_componmod(cmd, new_compon)) {
			// this is not graphical
			lang_geda._componentname=new_compon->short_label();
			trace2("do_it, componmod", lang_geda._componentname, cmd.fullstring());
			try{
				LANG_GEDA::find_nondevice(new_compon->short_label(), Scope);
				delete clone;
			}catch(Exception_Cant_Find const&){
				CARD_LIST::card_list.push_back(new_compon);
			}

			cmd.reset();
			trace1("not calling new__instance ", cmd.fullstring());
			lang_geda._gotline = true;
		} else { untested();
			unreachable();
			delete clone;
		}

	}
} p2;
DISPATCHER<CMD>::INSTALL
d2(&command_dispatcher, "gC", &p2);
/*----------------------------------------------------------------------*/
std::string symbolpath = ".";
/*----------------------------------------------------------------------*/
class CMD_SYMBOLPATH : public CMD {
public:
  void do_it(CS& cmd, CARD_LIST* Scope)override {
    cmd >> symbolpath;
  }
}p1;
DISPATCHER<CMD>::INSTALL d1(&command_dispatcher, "set_symbolpath", &p1);
/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
} //geda
/*----------------------------------------------------------------------*/
// transition..
GEDA_SYMBOL* make_GEDA_SYMBOL(std::string basename)
{
	std::string filename = findfile(basename, geda::symbolpath, R_OK);
	trace3( "GEDA_SYMBOL::GEDA_SYMBOL", basename, filename, geda::symbolpath);

#ifdef HAVE_LIBGEDA_LIBGEDA_H
	if(filename==""){ untested();
		const CLibSymbol* symbol = s_clib_get_symbol_by_name(basename.c_str());
		if(!symbol){ untested();
			trace1("error fetching", basename);
			throw(Exception_Cant_Find("parsing gedanetlist", basename));
		}else{ untested();
		}
		filename = s_clib_symbol_get_filename(symbol);
	}else{ untested();
	}
#else
	{
	}
#endif
	if(filename==""){ untested();
		throw(Exception_Cant_Find("parsing gedanetlist", basename));
	}else{
	}
	trace1("...", filename);
	CS cmd(CS::_INC_FILE, filename);
	cmd.get_line("");
	CARD* c = device_dispatcher.clone("geda_symbol");
	assert(c);
	GEDA_SYMBOL* s = prechecked_cast<GEDA_SYMBOL*>(c);
	assert(s);
	geda::lang_geda.parse_symbol_file(s, cmd);

	trace2("done parse", basename, s->_pins.size());
	return s;
}
/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
