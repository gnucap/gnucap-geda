#!/bin/sh -x

what=osc

export LD_LIBRARY_PATH=${PWD}/../src/.libs:${LD_LIBARY_PATH}

cp -r $srcdir/../examples/$what .
(cd $what; ln -s ../$srcdir/symbols)
chmod -R ug+w $what
cp -r $srcdir/../include/analog.v $what
cp -r $srcdir/../include/spice.v $what
(cd $what; gnucap -a lang_geda.so -c set_includepath\ . transient.gc) | tail -n +20 > check/$what.out
cat $what/tr.out >> check/$what.out

diff check/$what.out $srcdir/ref/$what.out
