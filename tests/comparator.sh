#!/bin/sh -x

what=comparator

export LD_LIBRARY_PATH=${PWD}/../src/.libs:${LD_LIBARY_PATH}

cp -r $srcdir/../examples/$what .
(cd $what; ln -s ../$srcdir/symbols)
chmod -R ug+w $what
cp -r $srcdir/../include/analog.v $what
(cd $what; gnucap transient.gc) | tail -n +20 > check/$what.out
cat $what/tr.out >> check/$what.out

diff check/$what.out $srcdir/ref/$what.out
